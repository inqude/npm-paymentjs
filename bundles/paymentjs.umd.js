(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common/http'), require('uuid'), require('url-search-params-polyfill'), require('ngx-smart-modal'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('paymentjs', ['exports', '@angular/core', '@angular/common/http', 'uuid', 'url-search-params-polyfill', 'ngx-smart-modal', '@angular/common'], factory) :
    (global = global || self, factory(global.paymentjs = {}, global.ng.core, global.ng.common.http, global.uuid, null, global.ngxSmartModal, global.ng.common));
}(this, (function (exports, core, http, uuid, urlSearchParamsPolyfill, ngxSmartModal, common) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var StripeService = /** @class */ (function () {
        function StripeService(http) {
            this.http = http;
            this.isAllowed = false;
            this.addScript = false;
            this.scriptReady = true;
            //  this.__init();
        }
        /**
         * @return {?}
         */
        StripeService.prototype.__init = /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (!this.addScript && this.isAllowed) {
                this.injectScript().then((/**
                 * @return {?}
                 */
                function () {
                    _this.scriptReady = false;
                    console.log('stripe.js script ready');
                }));
            }
        };
        /**
         * @return {?}
         */
        StripeService.prototype.injectScript = /**
         * @return {?}
         */
        function () {
            this.addScript = true;
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                /** @type {?} */
                var scripttagElement = document.createElement('script');
                scripttagElement.src = 'https://checkout.stripe.com/checkout.js';
                scripttagElement.onload = resolve;
                scripttagElement.onerror = reject;
                document.body.appendChild(scripttagElement);
            }));
        };
        /**
         * @param {?} params
         * @return {?}
         */
        StripeService.prototype.fakeCharge = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                /** @type {?} */
                var total_amount = parseInt(params["amount"]);
                /** @type {?} */
                var fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
                /** @type {?} */
                var net_amount = (total_amount - fee_amount).toFixed(2);
                resolve({
                    "trxn_id": "ch_" + uuid.v4(),
                    "total_amount": total_amount,
                    "fee_amount": fee_amount,
                    "net_amount": net_amount,
                    "currency": "usd"
                });
            }));
        };
        /**
         * @param {?} params
         * @return {?}
         */
        StripeService.prototype.charge = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            /** @type {?} */
            var self = this;
            params.amount = (params.amount || 0) * 100;
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                self.handler = window.StripeCheckout.configure({
                    key: params.username,
                    locale: 'auto',
                    name: params.name || '',
                    image: params.image || '',
                    billingAddress: false,
                    zipCode: false,
                    allowRememberMe: false,
                    description: params.description || '',
                    closed: (/**
                     * @param {?} e
                     * @return {?}
                     */
                    function (e) {
                        if (!self.handler.isTokenGenerate) {
                            reject({
                                "code": "stripe_transaction_cancelled"
                            });
                        }
                    }),
                    token: (/**
                     * @param {?} token
                     * @return {?}
                     */
                    function (token) {
                        resolve({
                            "total_amount": (params.amount / 100),
                            "fee_amount": 0,
                            "net_amount": (params.amount / 100),
                            "currency": params.currency,
                            "trxn_id": token.id
                        });
                        return false;
                    })
                });
                self.handler.open(params);
            }));
        };
        /**
         * @return {?}
         */
        StripeService.prototype.onPopstate = /**
         * @return {?}
         */
        function () {
            this.handler.close();
        };
        StripeService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        StripeService.ctorParameters = function () { return [
            { type: http.HttpClient }
        ]; };
        StripeService.propDecorators = {
            onPopstate: [{ type: core.HostListener, args: ['window:popstate',] }]
        };
        return StripeService;
    }());
    if (false) {
        /** @type {?} */
        StripeService.prototype.isAllowed;
        /** @type {?} */
        StripeService.prototype.addScript;
        /** @type {?} */
        StripeService.prototype.scriptReady;
        /** @type {?} */
        StripeService.prototype.handler;
        /**
         * @type {?}
         * @private
         */
        StripeService.prototype.http;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var RazorpayService = /** @class */ (function () {
        function RazorpayService() {
            this.isAllowed = false;
            this.addScript = false;
            this.scriptReady = true;
            //  this.__init();
        }
        /**
         * @return {?}
         */
        RazorpayService.prototype.__init = /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (!this.addScript && this.isAllowed) {
                this.injectScript().then((/**
                 * @return {?}
                 */
                function () {
                    _this.scriptReady = false;
                    console.log('razorpay.js script ready');
                }));
            }
        };
        /**
         * @return {?}
         */
        RazorpayService.prototype.injectScript = /**
         * @return {?}
         */
        function () {
            this.addScript = true;
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                /** @type {?} */
                var scripttagElement = document.createElement('script');
                scripttagElement.src = 'https://checkout.razorpay.com/v1/checkout.js';
                scripttagElement.onload = resolve;
                scripttagElement.onerror = reject;
                document.body.appendChild(scripttagElement);
            }));
        };
        /**
         * @param {?} params
         * @return {?}
         */
        RazorpayService.prototype.fakeCharge = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                /** @type {?} */
                var total_amount = parseInt(params["amount"]);
                /** @type {?} */
                var fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
                /** @type {?} */
                var net_amount = (total_amount - fee_amount).toFixed(2);
                resolve({
                    "trxn_id": "ch_" + uuid.v4(),
                    "total_amount": total_amount,
                    "fee_amount": fee_amount,
                    "net_amount": net_amount,
                    "currency": "usd"
                });
            }));
        };
        /**
         * @param {?} params
         * @return {?}
         */
        RazorpayService.prototype.charge = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            /** @type {?} */
            var self = this;
            /** @type {?} */
            var amount = (params.amount || 0) * 100;
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                /** @type {?} */
                var options = {
                    "key": params.username,
                    "amount": amount,
                    "currency": params.currency || "INR",
                    "name": params.name || '',
                    "description": params.description,
                    "image": params.image || '',
                    "prefill": {
                        "email": params.email || '',
                        "contact": params.phonenumber || ''
                    },
                    "modal": {
                        "ondismiss": (/**
                         * @return {?}
                         */
                        function () {
                            reject('Razorpay modal closed');
                        })
                    },
                    handler: (/**
                     * @param {?} token
                     * @return {?}
                     */
                    function (token) {
                        resolve({
                            "total_amount": params.amount,
                            "fee_amount": 0,
                            "net_amount": params.amount,
                            "currency": params.currency,
                            "trxn_id": token.razorpay_payment_id,
                            "raw_response": token
                        });
                        return false;
                    })
                }
                // optional
                ;
                // optional
                if (params.order_id) {
                    options.order_id = params.order_id;
                }
                self.handler = new window.Razorpay(options);
                self.handler.open();
            }));
        };
        /**
         * @return {?}
         */
        RazorpayService.prototype.onPopstate = /**
         * @return {?}
         */
        function () {
            this.handler.close();
        };
        RazorpayService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        RazorpayService.ctorParameters = function () { return []; };
        RazorpayService.propDecorators = {
            onPopstate: [{ type: core.HostListener, args: ['window:popstate',] }]
        };
        return RazorpayService;
    }());
    if (false) {
        /** @type {?} */
        RazorpayService.prototype.isAllowed;
        /** @type {?} */
        RazorpayService.prototype.addScript;
        /** @type {?} */
        RazorpayService.prototype.scriptReady;
        /** @type {?} */
        RazorpayService.prototype.handler;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PaypalModalComponent = /** @class */ (function () {
        function PaypalModalComponent(modalService) {
            this.modalService = modalService;
            this.data = {};
        }
        /**
         * @return {?}
         */
        PaypalModalComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            /** @type {?} */
            var modalRef = this.modalService.getTopOpenedModal();
            modalRef.onDataAdded.subscribe((/**
             * @param {?} data
             * @return {?}
             */
            function (data) {
                _this.data = data;
            }));
        };
        PaypalModalComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'paypal-modal-component',
                        template: "<div *ngIf=\"data.isTest\" style=\"position: fixed;left: 0;top: -23px;padding: 2px 10px;background-color: #3F51B5;color: white;font-size: 0.9rem;border-top-left-radius: 5px;border-top-right-radius: 5px;\">PayPal: Test mode</div>\n<h4 class=\"modal-title\" id=\"modal-basic-title\">PayPal Payment</h4>\n<div class=\"modal-body\" style=\"overflow: auto;\">\n  <div id=\"paypal-buttons-container\" style=\"max-height: 400px;\"></div>\n</div>\n",
                        styles: [".paypalModalClass { width: 360px; }"]
                    }] }
        ];
        /** @nocollapse */
        PaypalModalComponent.ctorParameters = function () { return [
            { type: ngxSmartModal.NgxSmartModalService }
        ]; };
        return PaypalModalComponent;
    }());
    if (false) {
        /** @type {?} */
        PaypalModalComponent.prototype.data;
        /** @type {?} */
        PaypalModalComponent.prototype.modalService;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PayPalService = /** @class */ (function () {
        function PayPalService(modalService) {
            this.modalService = modalService;
            this.isAllowed = false;
            this.addScript = false;
            this.scriptReady = true;
            this.__init();
        }
        /**
         * @return {?}
         */
        PayPalService.prototype.__init = /**
         * @return {?}
         */
        function () {
            var _this = this;
            if (!this.addScript && this.isAllowed) {
                this.injectScript().then((/**
                 * @return {?}
                 */
                function () {
                    _this.scriptReady = false;
                    console.log('PayPal checkout.js script ready');
                }));
            }
        };
        /**
         * @param {?} params
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        PayPalService.prototype.config = /**
         * @param {?} params
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (params, resolve, reject) {
            var _this = this;
            return {
                "locale": 'en_US',
                "env": params.isTest ? 'sandbox' : 'production',
                "style": {
                    "layout": 'vertical',
                },
                "client": {
                    "sandbox": params.username,
                    "production": params.username,
                },
                "commit": true,
                "payment": (/**
                 * @param {?} data
                 * @param {?} actions
                 * @return {?}
                 */
                function (data, actions) {
                    return actions.payment.create({
                        "payment": {
                            "transactions": [
                                {
                                    "amount": {
                                        "total": params.amount,
                                        "currency": params.currency
                                    },
                                    "description": params.description || ''
                                },
                            ],
                        },
                    });
                }),
                onAuthorize: (/**
                 * @param {?} data
                 * @param {?} actions
                 * @return {?}
                 */
                function (data, actions) {
                    return actions.payment
                        .execute()
                        .then((/**
                     * @param {?} payment
                     * @return {?}
                     */
                    function (payment) {
                        /** @type {?} */
                        var total_amount = params.amount;
                        /** @type {?} */
                        var fee_amount = 0;
                        /** @type {?} */
                        var net_amount = params.amount;
                        // check if the PayPal transaction didn't went through
                        if (payment.intent != 'sale' && payment.state != 'approved') {
                            reject(payment);
                        }
                        else {
                            resolve({
                                total_amount: total_amount,
                                fee_amount: fee_amount,
                                net_amount: net_amount,
                                currency: params.currency,
                                trxn_id: payment.id,
                            });
                            if (_this.modalRef && _this.modalRef.close) {
                                _this.modalRef.close();
                            }
                        }
                    }))
                        .catch((/**
                     * @param {?} e
                     * @return {?}
                     */
                    function (e) {
                        console.log(e);
                        reject(null);
                    }));
                }),
            };
        };
        /**
         * @return {?}
         */
        PayPalService.prototype.injectScript = /**
         * @return {?}
         */
        function () {
            this.addScript = true;
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                /** @type {?} */
                var scripttagElement = document.createElement('script');
                scripttagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
                scripttagElement.onload = resolve;
                scripttagElement.onerror = reject;
                document.body.appendChild(scripttagElement);
            }));
        };
        /**
         * @param {?} params
         * @return {?}
         */
        PayPalService.prototype.fakeCharge = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                /** @type {?} */
                var total_amount = parseInt(params['amount']);
                /** @type {?} */
                var fee_amount = ((100 * 2.9) / total_amount + 0.3).toFixed(2);
                /** @type {?} */
                var net_amount = (total_amount - fee_amount).toFixed(2);
                resolve({
                    trxn_id: 'ch_' + uuid.v4(),
                    total_amount: total_amount,
                    fee_amount: fee_amount,
                    net_amount: net_amount,
                    currency: 'usd',
                });
            }));
        };
        /**
         * @param {?} params
         * @return {?}
         */
        PayPalService.prototype.charge = /**
         * @param {?} params
         * @return {?}
         */
        function (params) {
            var _this = this;
            /** @type {?} */
            var self = this;
            params.amount = params.amount || 0;
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) {
                if (!_this.addScript) {
                    console.error('PayPal checkout.js script is not ready');
                    return false;
                }
                /** @type {?} */
                var modalOptions = {
                    escapable: true,
                    customClass: 'paypalModalClass'
                };
                _this.modalRef = _this.modalService.create('PaypalModal', PaypalModalComponent, modalOptions).open();
                _this.modalRef.onOpenFinished.subscribe((/**
                 * @param {?} result
                 * @return {?}
                 */
                function (result) {
                    _this.modalService.setModalData({ isTest: params.isTest }, 'PaypalModal');
                }));
                _this.modalRef.onClose.subscribe((/**
                 * @param {?} result
                 * @return {?}
                 */
                function (result) {
                    console.log('closed', result);
                    reject();
                }));
                window.setTimeout((/**
                 * @return {?}
                 */
                function () {
                    window.paypal.Button.render(self.config(params, resolve, reject), '#paypal-buttons-container');
                }), 1000);
            }));
        };
        PayPalService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        PayPalService.ctorParameters = function () { return [
            { type: ngxSmartModal.NgxSmartModalService }
        ]; };
        return PayPalService;
    }());
    if (false) {
        /** @type {?} */
        PayPalService.prototype.isAllowed;
        /** @type {?} */
        PayPalService.prototype.addScript;
        /** @type {?} */
        PayPalService.prototype.scriptReady;
        /** @type {?} */
        PayPalService.prototype.handler;
        /**
         * @type {?}
         * @private
         */
        PayPalService.prototype.modalRef;
        /**
         * @type {?}
         * @private
         */
        PayPalService.prototype.modalService;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PaymentjsService = /** @class */ (function () {
        function PaymentjsService(stripe, razorpay, paypal) {
            this.stripe = stripe;
            this.razorpay = razorpay;
            this.paypal = paypal;
        }
        /**
         * @param {?} className
         * @return {?}
         */
        PaymentjsService.prototype.initialize = /**
         * @param {?} className
         * @return {?}
         */
        function (className) {
            /** @type {?} */
            var paymentIntrument = (className || '').toLowerCase().split('_')[1];
            this[paymentIntrument].isAllowed = true;
            this[paymentIntrument].__init();
        };
        /**
         * @param {?} data
         * @return {?}
         */
        PaymentjsService.prototype.checkout = /**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            var _this = this;
            /** @type {?} */
            var result;
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
                var payment_type, _a, e_1;
                return __generator(this, function (_b) {
                    switch (_b.label) {
                        case 0:
                            payment_type = data.paymentInstrument.toLowerCase();
                            _b.label = 1;
                        case 1:
                            _b.trys.push([1, 10, , 11]);
                            _a = payment_type;
                            switch (_a) {
                                case 'payment_stripe': return [3 /*break*/, 2];
                                case 'payment_razorpay': return [3 /*break*/, 4];
                                case 'payment_paypal': return [3 /*break*/, 6];
                            }
                            return [3 /*break*/, 8];
                        case 2: return [4 /*yield*/, this.stripe.charge(data.params)];
                        case 3:
                            result = _b.sent();
                            return [3 /*break*/, 9];
                        case 4: return [4 /*yield*/, this.razorpay.charge(data.params)];
                        case 5:
                            result = _b.sent();
                            return [3 /*break*/, 9];
                        case 6: return [4 /*yield*/, this.paypal.charge(data.params)];
                        case 7:
                            result = _b.sent();
                            return [3 /*break*/, 9];
                        case 8:
                            reject("Unsupported Payment Instrument " + data.paymentInstrument);
                            return [2 /*return*/, false];
                        case 9:
                            result["isTest"] = data.params.isTest;
                            result["paymentInstrument"] = data.paymentInstrument;
                            resolve(result);
                            return [3 /*break*/, 11];
                        case 10:
                            e_1 = _b.sent();
                            console.log(e_1);
                            reject('Transaction cancelled!');
                            return [3 /*break*/, 11];
                        case 11: return [2 /*return*/];
                    }
                });
            }); }));
        };
        PaymentjsService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        PaymentjsService.ctorParameters = function () { return [
            { type: StripeService },
            { type: RazorpayService },
            { type: PayPalService }
        ]; };
        return PaymentjsService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        PaymentjsService.prototype.stripe;
        /**
         * @type {?}
         * @private
         */
        PaymentjsService.prototype.razorpay;
        /**
         * @type {?}
         * @private
         */
        PaymentjsService.prototype.paypal;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PaymentjsComponent = /** @class */ (function () {
        function PaymentjsComponent() {
        }
        /**
         * @return {?}
         */
        PaymentjsComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
        };
        PaymentjsComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'lib-paymentjs',
                        template: "\n    <p>\n      paymentjs works!\n    </p>\n  "
                    }] }
        ];
        /** @nocollapse */
        PaymentjsComponent.ctorParameters = function () { return []; };
        return PaymentjsComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var PaymentjsModule = /** @class */ (function () {
        function PaymentjsModule() {
        }
        PaymentjsModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [PaymentjsComponent, PaypalModalComponent],
                        imports: [common.CommonModule, ngxSmartModal.NgxSmartModalModule.forRoot()],
                        entryComponents: [PaypalModalComponent],
                        providers: [PaymentjsService, StripeService, RazorpayService, PayPalService],
                        exports: [PaymentjsComponent, PaypalModalComponent]
                    },] }
        ];
        return PaymentjsModule;
    }());

    exports.PaymentjsComponent = PaymentjsComponent;
    exports.PaymentjsModule = PaymentjsModule;
    exports.PaymentjsService = PaymentjsService;
    exports.ɵa = StripeService;
    exports.ɵb = RazorpayService;
    exports.ɵc = PayPalService;
    exports.ɵd = PaypalModalComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=paymentjs.umd.js.map
