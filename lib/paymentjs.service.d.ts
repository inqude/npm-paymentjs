import { StripeService } from '../vendor/stripe';
import { RazorpayService } from '../vendor/razorpay';
import { PayPalService } from '../vendor/paypal';
export declare class PaymentjsService {
    private stripe;
    private razorpay;
    private paypal;
    constructor(stripe: StripeService, razorpay: RazorpayService, paypal: PayPalService);
    initialize(className: string): void;
    checkout(data: any): Promise<unknown>;
}
