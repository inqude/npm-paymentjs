/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { StripeService } from '../vendor/stripe';
import { RazorpayService } from '../vendor/razorpay';
import { PayPalService } from '../vendor/paypal';
export class PaymentjsService {
    /**
     * @param {?} stripe
     * @param {?} razorpay
     * @param {?} paypal
     */
    constructor(stripe, razorpay, paypal) {
        this.stripe = stripe;
        this.razorpay = razorpay;
        this.paypal = paypal;
    }
    /**
     * @param {?} className
     * @return {?}
     */
    initialize(className) {
        /** @type {?} */
        const paymentIntrument = (className || '').toLowerCase().split('_')[1];
        this[paymentIntrument].isAllowed = true;
        this[paymentIntrument].__init();
    }
    /**
     * @param {?} data
     * @return {?}
     */
    checkout(data) {
        /** @type {?} */
        let result;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const payment_type = data.paymentInstrument.toLowerCase();
            try {
                switch (payment_type) {
                    case 'payment_stripe':
                        result = yield this.stripe.charge(data.params);
                        break;
                    case 'payment_razorpay':
                        result = yield this.razorpay.charge(data.params);
                        break;
                    case 'payment_paypal':
                        result = yield this.paypal.charge(data.params);
                        break;
                    default:
                        reject(`Unsupported Payment Instrument ${data.paymentInstrument}`);
                        return false;
                        break;
                }
                result["isTest"] = data.params.isTest;
                result["paymentInstrument"] = data.paymentInstrument;
                resolve(result);
            }
            catch (e) {
                console.log(e);
                reject('Transaction cancelled!');
            }
        })));
    }
}
PaymentjsService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
PaymentjsService.ctorParameters = () => [
    { type: StripeService },
    { type: RazorpayService },
    { type: PayPalService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.stripe;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.razorpay;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.paypal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5bWVudGpzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYXltZW50anMvIiwic291cmNlcyI6WyJsaWIvcGF5bWVudGpzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBR2pELE1BQU0sT0FBTyxnQkFBZ0I7Ozs7OztJQUMzQixZQUNVLE1BQXFCLEVBQ3JCLFFBQXlCLEVBQ3pCLE1BQXFCO1FBRnJCLFdBQU0sR0FBTixNQUFNLENBQWU7UUFDckIsYUFBUSxHQUFSLFFBQVEsQ0FBaUI7UUFDekIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtJQUMzQixDQUFDOzs7OztJQUVMLFVBQVUsQ0FBQyxTQUFpQjs7Y0FDcEIsZ0JBQWdCLEdBQVcsQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM5RSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2xDLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLElBQVM7O1lBQ1osTUFBVztRQUNmLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLENBQU0sT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFOztrQkFDcEMsWUFBWSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUU7WUFDekQsSUFBSTtnQkFDRixRQUFRLFlBQVksRUFBRTtvQkFDcEIsS0FBSyxnQkFBZ0I7d0JBQ25CLE1BQU0sR0FBRyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDL0MsTUFBTTtvQkFFUixLQUFLLGtCQUFrQjt3QkFDckIsTUFBTSxHQUFHLE1BQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3dCQUNqRCxNQUFNO29CQUVSLEtBQUssZ0JBQWdCO3dCQUNuQixNQUFNLEdBQUcsTUFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7d0JBQy9DLE1BQU07b0JBRVI7d0JBQ0UsTUFBTSxDQUFDLGtDQUFrQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLEtBQUssQ0FBQzt3QkFDYixNQUFNO2lCQUNUO2dCQUNELE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztnQkFDdEMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO2dCQUNyRCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDakI7WUFBQyxPQUFPLENBQUMsRUFBRTtnQkFDVixPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNmLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO2FBQ2xDO1FBQ0gsQ0FBQyxDQUFBLEVBQUMsQ0FBQTtJQUNKLENBQUM7OztZQTdDRixVQUFVOzs7O1lBSkYsYUFBYTtZQUNiLGVBQWU7WUFDZixhQUFhOzs7Ozs7O0lBS2xCLGtDQUE2Qjs7Ozs7SUFDN0Isb0NBQWlDOzs7OztJQUNqQyxrQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTdHJpcGVTZXJ2aWNlIH0gZnJvbSAnLi4vdmVuZG9yL3N0cmlwZSc7XG5pbXBvcnQgeyBSYXpvcnBheVNlcnZpY2UgfSBmcm9tICcuLi92ZW5kb3IvcmF6b3JwYXknO1xuaW1wb3J0IHsgUGF5UGFsU2VydmljZSB9IGZyb20gJy4uL3ZlbmRvci9wYXlwYWwnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgUGF5bWVudGpzU2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgc3RyaXBlOiBTdHJpcGVTZXJ2aWNlLFxuICAgIHByaXZhdGUgcmF6b3JwYXk6IFJhem9ycGF5U2VydmljZSxcbiAgICBwcml2YXRlIHBheXBhbDogUGF5UGFsU2VydmljZSxcbiAgKSB7IH1cbiAgXG4gIGluaXRpYWxpemUoY2xhc3NOYW1lOiBzdHJpbmcpe1xuICAgIGNvbnN0IHBheW1lbnRJbnRydW1lbnQ6IHN0cmluZyA9IChjbGFzc05hbWUgfHwgJycpLnRvTG93ZXJDYXNlKCkuc3BsaXQoJ18nKVsxXTtcbiAgICB0aGlzW3BheW1lbnRJbnRydW1lbnRdLmlzQWxsb3dlZCA9IHRydWU7XG4gICAgdGhpc1twYXltZW50SW50cnVtZW50XS5fX2luaXQoKTtcbiAgfVxuXG4gIGNoZWNrb3V0KGRhdGE6IGFueSl7XG4gICAgbGV0IHJlc3VsdDogYW55O1xuICAgIHJldHVybiBuZXcgUHJvbWlzZShhc3luYyhyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGNvbnN0IHBheW1lbnRfdHlwZSA9IGRhdGEucGF5bWVudEluc3RydW1lbnQudG9Mb3dlckNhc2UoKTtcbiAgICAgIHRyeSB7XG4gICAgICAgIHN3aXRjaCAocGF5bWVudF90eXBlKSB7XG4gICAgICAgICAgY2FzZSAncGF5bWVudF9zdHJpcGUnOlxuICAgICAgICAgICAgcmVzdWx0ID0gYXdhaXQgdGhpcy5zdHJpcGUuY2hhcmdlKGRhdGEucGFyYW1zKTtcbiAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgY2FzZSAncGF5bWVudF9yYXpvcnBheSc6XG4gICAgICAgICAgICByZXN1bHQgPSBhd2FpdCB0aGlzLnJhem9ycGF5LmNoYXJnZShkYXRhLnBhcmFtcyk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgIGNhc2UgJ3BheW1lbnRfcGF5cGFsJzpcbiAgICAgICAgICAgIHJlc3VsdCA9IGF3YWl0IHRoaXMucGF5cGFsLmNoYXJnZShkYXRhLnBhcmFtcyk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICByZWplY3QoYFVuc3VwcG9ydGVkIFBheW1lbnQgSW5zdHJ1bWVudCAke2RhdGEucGF5bWVudEluc3RydW1lbnR9YCk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgICByZXN1bHRbXCJpc1Rlc3RcIl0gPSBkYXRhLnBhcmFtcy5pc1Rlc3Q7XG4gICAgICAgIHJlc3VsdFtcInBheW1lbnRJbnN0cnVtZW50XCJdID0gZGF0YS5wYXltZW50SW5zdHJ1bWVudDtcbiAgICAgICAgcmVzb2x2ZShyZXN1bHQpO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICBjb25zb2xlLmxvZyhlKTtcbiAgICAgICAgcmVqZWN0KCdUcmFuc2FjdGlvbiBjYW5jZWxsZWQhJyk7XG4gICAgICB9XG4gICAgfSlcbiAgfVxufVxuIl19