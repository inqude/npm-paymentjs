/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { PaymentjsComponent } from './paymentjs.component';
import { PaymentjsService } from './paymentjs.service';
import { StripeService } from '../vendor/stripe';
import { RazorpayService } from '../vendor/razorpay';
import { PayPalService } from '../vendor/paypal';
import { PaypalModalComponent } from '../vendor/paypal.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { CommonModule } from '@angular/common';
export class PaymentjsModule {
}
PaymentjsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [PaymentjsComponent, PaypalModalComponent],
                imports: [CommonModule, NgxSmartModalModule.forRoot()],
                entryComponents: [PaypalModalComponent],
                providers: [PaymentjsService, StripeService, RazorpayService, PayPalService],
                exports: [PaymentjsComponent, PaypalModalComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5bWVudGpzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BheW1lbnRqcy8iLCJzb3VyY2VzIjpbImxpYi9wYXltZW50anMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzNELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQVMvQyxNQUFNLE9BQU8sZUFBZTs7O1lBUDNCLFFBQVEsU0FBQztnQkFDUixZQUFZLEVBQUUsQ0FBQyxrQkFBa0IsRUFBRSxvQkFBb0IsQ0FBQztnQkFDeEQsT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLG1CQUFtQixDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUN0RCxlQUFlLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztnQkFDdkMsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsYUFBYSxFQUFFLGVBQWUsRUFBRSxhQUFhLENBQUM7Z0JBQzVFLE9BQU8sRUFBRSxDQUFDLGtCQUFrQixFQUFFLG9CQUFvQixDQUFDO2FBQ3BEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFBheW1lbnRqc0NvbXBvbmVudCB9IGZyb20gJy4vcGF5bWVudGpzLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBQYXltZW50anNTZXJ2aWNlIH0gZnJvbSAnLi9wYXltZW50anMuc2VydmljZSc7XG5pbXBvcnQgeyBTdHJpcGVTZXJ2aWNlIH0gZnJvbSAnLi4vdmVuZG9yL3N0cmlwZSc7XG5pbXBvcnQgeyBSYXpvcnBheVNlcnZpY2UgfSBmcm9tICcuLi92ZW5kb3IvcmF6b3JwYXknO1xuaW1wb3J0IHsgUGF5UGFsU2VydmljZSB9IGZyb20gJy4uL3ZlbmRvci9wYXlwYWwnO1xuaW1wb3J0IHsgUGF5cGFsTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi92ZW5kb3IvcGF5cGFsLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBOZ3hTbWFydE1vZGFsTW9kdWxlIH0gZnJvbSAnbmd4LXNtYXJ0LW1vZGFsJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1BheW1lbnRqc0NvbXBvbmVudCwgUGF5cGFsTW9kYWxDb21wb25lbnRdLFxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlLCBOZ3hTbWFydE1vZGFsTW9kdWxlLmZvclJvb3QoKV0sXG4gIGVudHJ5Q29tcG9uZW50czogW1BheXBhbE1vZGFsQ29tcG9uZW50XSxcbiAgcHJvdmlkZXJzOiBbUGF5bWVudGpzU2VydmljZSwgU3RyaXBlU2VydmljZSwgUmF6b3JwYXlTZXJ2aWNlLCBQYXlQYWxTZXJ2aWNlXSxcbiAgZXhwb3J0czogW1BheW1lbnRqc0NvbXBvbmVudCwgUGF5cGFsTW9kYWxDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIFBheW1lbnRqc01vZHVsZSB7IH1cbiJdfQ==