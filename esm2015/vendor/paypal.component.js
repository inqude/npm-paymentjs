/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
export class PaypalModalComponent {
    /**
     * @param {?} modalService
     */
    constructor(modalService) {
        this.modalService = modalService;
        this.data = {};
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const modalRef = this.modalService.getTopOpenedModal();
        modalRef.onDataAdded.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        (data) => {
            this.data = data;
        }));
    }
}
PaypalModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'paypal-modal-component',
                template: "<div *ngIf=\"data.isTest\" style=\"position: fixed;left: 0;top: -23px;padding: 2px 10px;background-color: #3F51B5;color: white;font-size: 0.9rem;border-top-left-radius: 5px;border-top-right-radius: 5px;\">PayPal: Test mode</div>\n<h4 class=\"modal-title\" id=\"modal-basic-title\">PayPal Payment</h4>\n<div class=\"modal-body\" style=\"overflow: auto;\">\n  <div id=\"paypal-buttons-container\" style=\"max-height: 400px;\"></div>\n</div>\n",
                styles: [".paypalModalClass { width: 360px; }"]
            }] }
];
/** @nocollapse */
PaypalModalComponent.ctorParameters = () => [
    { type: NgxSmartModalService }
];
if (false) {
    /** @type {?} */
    PaypalModalComponent.prototype.data;
    /** @type {?} */
    PaypalModalComponent.prototype.modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5cGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BheW1lbnRqcy8iLCJzb3VyY2VzIjpbInZlbmRvci9wYXlwYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBT3ZELE1BQU0sT0FBTyxvQkFBb0I7Ozs7SUFFL0IsWUFBbUIsWUFBa0M7UUFBbEMsaUJBQVksR0FBWixZQUFZLENBQXNCO1FBRDlDLFNBQUksR0FBUSxFQUFFLENBQUM7SUFDbUMsQ0FBQzs7OztJQUUxRCxRQUFROztjQUNBLFFBQVEsR0FBUSxJQUFJLENBQUMsWUFBWSxDQUFDLGlCQUFpQixFQUFFO1FBQzNELFFBQVEsQ0FBQyxXQUFXLENBQUMsU0FBUzs7OztRQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7WUFDM0MsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDbkIsQ0FBQyxFQUFDLENBQUE7SUFDSixDQUFDOzs7WUFkRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtnQkFDbEMsb2NBQTBDO3lCQUNqQyxxQ0FBcUM7YUFDL0M7Ozs7WUFOUSxvQkFBb0I7Ozs7SUFRM0Isb0NBQXNCOztJQUNWLDRDQUF5QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOZ3hTbWFydE1vZGFsU2VydmljZSB9IGZyb20gJ25neC1zbWFydC1tb2RhbCc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3BheXBhbC1tb2RhbC1jb21wb25lbnQnLFxuICB0ZW1wbGF0ZVVybDogJ3BheXBhbC1tb2RhbC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlczogW1wiLnBheXBhbE1vZGFsQ2xhc3MgeyB3aWR0aDogMzYwcHg7IH1cIl1cbn0pXG5leHBvcnQgY2xhc3MgUGF5cGFsTW9kYWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQgeyAgXG4gIHB1YmxpYyBkYXRhOiBhbnkgPSB7fTtcbiAgY29uc3RydWN0b3IocHVibGljIG1vZGFsU2VydmljZTogTmd4U21hcnRNb2RhbFNlcnZpY2UpIHsgfVxuXG4gIG5nT25Jbml0KCl7XG4gICAgY29uc3QgbW9kYWxSZWY6IGFueSA9IHRoaXMubW9kYWxTZXJ2aWNlLmdldFRvcE9wZW5lZE1vZGFsKCk7XG4gICAgbW9kYWxSZWYub25EYXRhQWRkZWQuc3Vic2NyaWJlKChkYXRhOiBhbnkpID0+IHtcbiAgICAgIHRoaXMuZGF0YSA9IGRhdGE7XG4gICAgfSkgICAgXG4gIH1cbn1cbiJdfQ==