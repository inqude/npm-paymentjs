/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, HostListener } from '@angular/core';
import { v4 as uuid } from 'uuid';
import 'url-search-params-polyfill';
export class RazorpayService {
    constructor() {
        this.isAllowed = false;
        this.addScript = false;
        this.scriptReady = true;
        //  this.__init();
    }
    /**
     * @return {?}
     */
    __init() {
        if (!this.addScript && this.isAllowed) {
            this.injectScript().then((/**
             * @return {?}
             */
            () => {
                this.scriptReady = false;
                console.log('razorpay.js script ready');
            }));
        }
    }
    /**
     * @return {?}
     */
    injectScript() {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.razorpay.com/v1/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    fakeCharge(params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let total_amount = parseInt(params["amount"]);
            /** @type {?} */
            let fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            let net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + uuid(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    charge(params) {
        /** @type {?} */
        let self = this;
        /** @type {?} */
        const amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let options = {
                "key": params.username,
                "amount": amount,
                "currency": params.currency || "INR",
                "name": params.name || '',
                "description": params.description,
                "image": params.image || '',
                "prefill": {
                    "email": params.email || '',
                    "contact": params.phonenumber || ''
                },
                "modal": {
                    "ondismiss": (/**
                     * @return {?}
                     */
                    function () {
                        reject('Razorpay modal closed');
                    })
                },
                handler: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": params.amount,
                        "fee_amount": 0,
                        "net_amount": params.amount,
                        "currency": params.currency,
                        "trxn_id": token.razorpay_payment_id,
                        "raw_response": token
                    });
                    return false;
                })
            }
            // optional
            ;
            // optional
            if (params.order_id) {
                options.order_id = params.order_id;
            }
            self.handler = new window.Razorpay(options);
            self.handler.open();
        }));
    }
    /**
     * @return {?}
     */
    onPopstate() {
        this.handler.close();
    }
}
RazorpayService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
RazorpayService.ctorParameters = () => [];
RazorpayService.propDecorators = {
    onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
};
if (false) {
    /** @type {?} */
    RazorpayService.prototype.isAllowed;
    /** @type {?} */
    RazorpayService.prototype.addScript;
    /** @type {?} */
    RazorpayService.prototype.scriptReady;
    /** @type {?} */
    RazorpayService.prototype.handler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmF6b3JwYXkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYXltZW50anMvIiwic291cmNlcyI6WyJ2ZW5kb3IvcmF6b3JwYXkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxFQUFFLElBQUksSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sNEJBQTRCLENBQUE7QUFJbkMsTUFBTSxPQUFPLGVBQWU7SUFNMUI7UUFMQSxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsZ0JBQVcsR0FBWSxJQUFJLENBQUM7UUFJMUIsa0JBQWtCO0lBQ2pCLENBQUM7Ozs7SUFFSixNQUFNO1FBQ0osSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSTs7O1lBQUMsR0FBRyxFQUFFO2dCQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztnQkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1lBQzFDLENBQUMsRUFBQyxDQUFBO1NBQ0g7SUFDSCxDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFOztnQkFDakMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7WUFDdkQsZ0JBQWdCLENBQUMsR0FBRyxHQUFHLDhDQUE4QyxDQUFDO1lBQ3RFLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7WUFDbEMsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztZQUNsQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzlDLENBQUMsRUFBQyxDQUFBO0lBQ0osQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsTUFBVztRQUNwQixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTs7Z0JBQ2pDLFlBQVksR0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDOztnQkFDN0MsVUFBVSxHQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOztnQkFDakUsVUFBVSxHQUFPLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFFM0QsT0FBTyxDQUFDO2dCQUNOLFNBQVMsRUFBRSxLQUFLLEdBQUcsSUFBSSxFQUFFO2dCQUN6QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsWUFBWSxFQUFFLFVBQVU7Z0JBQ3hCLFlBQVksRUFBRSxVQUFVO2dCQUN4QixVQUFVLEVBQUUsS0FBSzthQUNsQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQVc7O1lBQ1osSUFBSSxHQUFRLElBQUk7O2NBQ2QsTUFBTSxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsR0FBRyxHQUFHO1FBRXpDLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFOztnQkFDakMsT0FBTyxHQUFRO2dCQUNqQixLQUFLLEVBQUUsTUFBTSxDQUFDLFFBQVE7Z0JBQ3RCLFFBQVEsRUFBRSxNQUFNO2dCQUNoQixVQUFVLEVBQUUsTUFBTSxDQUFDLFFBQVEsSUFBSSxLQUFLO2dCQUNwQyxNQUFNLEVBQUUsTUFBTSxDQUFDLElBQUksSUFBSSxFQUFFO2dCQUN6QixhQUFhLEVBQUUsTUFBTSxDQUFDLFdBQVc7Z0JBQ2pDLE9BQU8sRUFBRSxNQUFNLENBQUMsS0FBSyxJQUFJLEVBQUU7Z0JBQzNCLFNBQVMsRUFBRTtvQkFDVCxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssSUFBSSxFQUFFO29CQUMzQixTQUFTLEVBQUUsTUFBTSxDQUFDLFdBQVcsSUFBSSxFQUFFO2lCQUNwQztnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsV0FBVzs7O29CQUFFO3dCQUNYLE1BQU0sQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBO29CQUNqQyxDQUFDLENBQUE7aUJBQ0Y7Z0JBQ0QsT0FBTzs7OztnQkFBRSxVQUFTLEtBQUs7b0JBQ3JCLE9BQU8sQ0FBQzt3QkFDTixjQUFjLEVBQUUsTUFBTSxDQUFDLE1BQU07d0JBQzdCLFlBQVksRUFBRSxDQUFDO3dCQUNmLFlBQVksRUFBRSxNQUFNLENBQUMsTUFBTTt3QkFDM0IsVUFBVSxFQUFFLE1BQU0sQ0FBQyxRQUFRO3dCQUMzQixTQUFTLEVBQUUsS0FBSyxDQUFDLG1CQUFtQjt3QkFDcEMsY0FBYyxFQUFFLEtBQUs7cUJBQ3RCLENBQUMsQ0FBQztvQkFDSCxPQUFPLEtBQUssQ0FBQztnQkFDZixDQUFDLENBQUE7YUFDRjtZQUVELFdBQVc7O1lBQVgsV0FBVztZQUNYLElBQUcsTUFBTSxDQUFDLFFBQVEsRUFBQztnQkFDakIsT0FBTyxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDO2FBQ3BDO1lBRUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDNUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN0QixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFHRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQTtJQUN0QixDQUFDOzs7WUE5RkYsVUFBVTs7Ozs7eUJBMkZSLFlBQVksU0FBQyxpQkFBaUI7Ozs7SUF6Ri9CLG9DQUEyQjs7SUFDM0Isb0NBQTJCOztJQUMzQixzQ0FBNEI7O0lBQzVCLGtDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSG9zdExpc3RlbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyB2NCBhcyB1dWlkIH0gZnJvbSAndXVpZCc7XG5pbXBvcnQgJ3VybC1zZWFyY2gtcGFyYW1zLXBvbHlmaWxsJ1xuZGVjbGFyZSB2YXIgd2luZG93OiBhbnk7XG7igItcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBSYXpvcnBheVNlcnZpY2Uge1xuICBpc0FsbG93ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgYWRkU2NyaXB0OiBib29sZWFuID0gZmFsc2U7XG4gIHNjcmlwdFJlYWR5OiBib29sZWFuID0gdHJ1ZTtcbiAgaGFuZGxlcjogYW55O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIC8vICB0aGlzLl9faW5pdCgpO1xuICAgICB9XG7igItcbiAgX19pbml0KCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5hZGRTY3JpcHQgJiYgdGhpcy5pc0FsbG93ZWQpIHtcbiAgICAgIHRoaXMuaW5qZWN0U2NyaXB0KCkudGhlbigoKSA9PiB7XG4gICAgICAgIHRoaXMuc2NyaXB0UmVhZHkgPSBmYWxzZTtcbiAgICAgICAgY29uc29sZS5sb2coJ3Jhem9ycGF5LmpzIHNjcmlwdCByZWFkeScpO1xuICAgICAgfSlcbiAgICB9XG4gIH1cblxuICBpbmplY3RTY3JpcHQoKSB7XG4gICAgdGhpcy5hZGRTY3JpcHQgPSB0cnVlO1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgc2NyaXB0dGFnRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5zcmMgPSAnaHR0cHM6Ly9jaGVja291dC5yYXpvcnBheS5jb20vdjEvY2hlY2tvdXQuanMnO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5vbmxvYWQgPSByZXNvbHZlO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5vbmVycm9yID0gcmVqZWN0O1xuICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChzY3JpcHR0YWdFbGVtZW50KTtcbiAgICB9KVxuICB9XG5cbiAgZmFrZUNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgdG90YWxfYW1vdW50OmFueSA9IHBhcnNlSW50KHBhcmFtc1tcImFtb3VudFwiXSk7XG4gICAgICBsZXQgZmVlX2Ftb3VudDphbnkgPSAoKCgxMDAgKiAyLjkpIC8gdG90YWxfYW1vdW50KSArIDAuMzApLnRvRml4ZWQoMik7XG4gICAgICBsZXQgbmV0X2Ftb3VudDphbnkgPSAodG90YWxfYW1vdW50IC0gZmVlX2Ftb3VudCkudG9GaXhlZCgyKTtcbuKAi1xuICAgICAgcmVzb2x2ZSh7XG4gICAgICAgIFwidHJ4bl9pZFwiOiBcImNoX1wiICsgdXVpZCgpLFxuICAgICAgICBcInRvdGFsX2Ftb3VudFwiOiB0b3RhbF9hbW91bnQsXG4gICAgICAgIFwiZmVlX2Ftb3VudFwiOiBmZWVfYW1vdW50LFxuICAgICAgICBcIm5ldF9hbW91bnRcIjogbmV0X2Ftb3VudCxcbiAgICAgICAgXCJjdXJyZW5jeVwiOiBcInVzZFwiXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxu4oCLXG4gIGNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIGxldCBzZWxmOiBhbnkgPSB0aGlzO1xuICAgIGNvbnN0IGFtb3VudCA9IChwYXJhbXMuYW1vdW50IHx8IDApICogMTAwXG5cbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgbGV0IG9wdGlvbnM6IGFueSA9IHtcbiAgICAgICAgXCJrZXlcIjogcGFyYW1zLnVzZXJuYW1lLFxuICAgICAgICBcImFtb3VudFwiOiBhbW91bnQsXG4gICAgICAgIFwiY3VycmVuY3lcIjogcGFyYW1zLmN1cnJlbmN5IHx8IFwiSU5SXCIsXG4gICAgICAgIFwibmFtZVwiOiBwYXJhbXMubmFtZSB8fCAnJyxcbiAgICAgICAgXCJkZXNjcmlwdGlvblwiOiBwYXJhbXMuZGVzY3JpcHRpb24sXG4gICAgICAgIFwiaW1hZ2VcIjogcGFyYW1zLmltYWdlIHx8ICcnLFxuICAgICAgICBcInByZWZpbGxcIjoge1xuICAgICAgICAgIFwiZW1haWxcIjogcGFyYW1zLmVtYWlsIHx8ICcnLFxuICAgICAgICAgIFwiY29udGFjdFwiOiBwYXJhbXMucGhvbmVudW1iZXIgfHwgJydcbiAgICAgICAgfSxcbiAgICAgICAgXCJtb2RhbFwiOiB7XG4gICAgICAgICAgXCJvbmRpc21pc3NcIjogZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIHJlamVjdCgnUmF6b3JwYXkgbW9kYWwgY2xvc2VkJylcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGhhbmRsZXI6IGZ1bmN0aW9uKHRva2VuKSB7XG4gICAgICAgICAgcmVzb2x2ZSh7XG4gICAgICAgICAgICBcInRvdGFsX2Ftb3VudFwiOiBwYXJhbXMuYW1vdW50LFxuICAgICAgICAgICAgXCJmZWVfYW1vdW50XCI6IDAsXG4gICAgICAgICAgICBcIm5ldF9hbW91bnRcIjogcGFyYW1zLmFtb3VudCxcbiAgICAgICAgICAgIFwiY3VycmVuY3lcIjogcGFyYW1zLmN1cnJlbmN5LFxuICAgICAgICAgICAgXCJ0cnhuX2lkXCI6IHRva2VuLnJhem9ycGF5X3BheW1lbnRfaWQsXG4gICAgICAgICAgICBcInJhd19yZXNwb25zZVwiOiB0b2tlbiAgICBcbiAgICAgICAgICB9KTtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7ICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIG9wdGlvbmFsXG4gICAgICBpZihwYXJhbXMub3JkZXJfaWQpe1xuICAgICAgICBvcHRpb25zLm9yZGVyX2lkID0gcGFyYW1zLm9yZGVyX2lkO1xuICAgICAgfVxuXG4gICAgICBzZWxmLmhhbmRsZXIgPSBuZXcgd2luZG93LlJhem9ycGF5KG9wdGlvbnMpO1xuICAgICAgc2VsZi5oYW5kbGVyLm9wZW4oKTtcbiAgICB9KTtcbiAgfVxu4oCLXG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpwb3BzdGF0ZScpXG4gIG9uUG9wc3RhdGUoKSB7XG4gICAgdGhpcy5oYW5kbGVyLmNsb3NlKClcbiAgfVxufSJdfQ==