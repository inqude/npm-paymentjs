/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { v4 as uuid } from 'uuid';
import { PaypalModalComponent } from './paypal.component';
import { NgxSmartModalService } from 'ngx-smart-modal';
import 'url-search-params-polyfill';
export class PayPalService {
    /**
     * @param {?} modalService
     */
    constructor(modalService) {
        this.modalService = modalService;
        this.isAllowed = false;
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    __init() {
        if (!this.addScript && this.isAllowed) {
            this.injectScript().then((/**
             * @return {?}
             */
            () => {
                this.scriptReady = false;
                console.log('PayPal checkout.js script ready');
            }));
        }
    }
    /**
     * @param {?} params
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    config(params, resolve, reject) {
        return {
            "locale": 'en_US',
            "env": params.isTest ? 'sandbox' : 'production',
            "style": {
                "layout": 'vertical',
            },
            "client": {
                "sandbox": params.username,
                "production": params.username,
            },
            "commit": true,
            "payment": (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            (data, actions) => {
                return actions.payment.create({
                    "payment": {
                        "transactions": [
                            {
                                "amount": {
                                    "total": params.amount,
                                    "currency": params.currency
                                },
                                "description": params.description || ''
                            },
                        ],
                    },
                });
            }),
            onAuthorize: (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            (data, actions) => {
                return actions.payment
                    .execute()
                    .then((/**
                 * @param {?} payment
                 * @return {?}
                 */
                payment => {
                    /** @type {?} */
                    let total_amount = params.amount;
                    /** @type {?} */
                    let fee_amount = 0;
                    /** @type {?} */
                    let net_amount = params.amount;
                    // check if the PayPal transaction didn't went through
                    if (payment.intent != 'sale' && payment.state != 'approved') {
                        reject(payment);
                    }
                    else {
                        resolve({
                            total_amount: total_amount,
                            fee_amount: fee_amount,
                            net_amount: net_amount,
                            currency: params.currency,
                            trxn_id: payment.id,
                        });
                        if (this.modalRef && this.modalRef.close) {
                            this.modalRef.close();
                        }
                    }
                }))
                    .catch((/**
                 * @param {?} e
                 * @return {?}
                 */
                e => {
                    console.log(e);
                    reject(null);
                }));
            }),
        };
    }
    /**
     * @return {?}
     */
    injectScript() {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    fakeCharge(params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let total_amount = parseInt(params['amount']);
            /** @type {?} */
            let fee_amount = ((100 * 2.9) / total_amount + 0.3).toFixed(2);
            /** @type {?} */
            let net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                trxn_id: 'ch_' + uuid(),
                total_amount: total_amount,
                fee_amount: fee_amount,
                net_amount: net_amount,
                currency: 'usd',
            });
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    charge(params) {
        /** @type {?} */
        let self = this;
        params.amount = params.amount || 0;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            if (!this.addScript) {
                console.error('PayPal checkout.js script is not ready');
                return false;
            }
            /** @type {?} */
            const modalOptions = {
                escapable: true,
                customClass: 'paypalModalClass'
            };
            this.modalRef = this.modalService.create('PaypalModal', PaypalModalComponent, modalOptions).open();
            this.modalRef.onOpenFinished.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                this.modalService.setModalData({ isTest: params.isTest }, 'PaypalModal');
            }));
            this.modalRef.onClose.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                console.log('closed', result);
                reject();
            }));
            window.setTimeout((/**
             * @return {?}
             */
            () => {
                window.paypal.Button.render(self.config(params, resolve, reject), '#paypal-buttons-container');
            }), 1000);
        }));
    }
}
PayPalService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
PayPalService.ctorParameters = () => [
    { type: NgxSmartModalService }
];
if (false) {
    /** @type {?} */
    PayPalService.prototype.isAllowed;
    /** @type {?} */
    PayPalService.prototype.addScript;
    /** @type {?} */
    PayPalService.prototype.scriptReady;
    /** @type {?} */
    PayPalService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalRef;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5cGFsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGF5bWVudGpzLyIsInNvdXJjZXMiOlsidmVuZG9yL3BheXBhbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsRUFBRSxJQUFJLElBQUksRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUNoQyxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUV2RCxPQUFPLDRCQUE0QixDQUFDO0FBR3BDLE1BQU0sT0FBTyxhQUFhOzs7O0lBT3hCLFlBQW9CLFlBQWtDO1FBQWxDLGlCQUFZLEdBQVosWUFBWSxDQUFzQjtRQU50RCxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsZ0JBQVcsR0FBWSxJQUFJLENBQUM7UUFLMUIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2hCLENBQUM7Ozs7SUFDRCxNQUFNO1FBQ0osSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSTs7O1lBQUMsR0FBRyxFQUFFO2dCQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztnQkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1lBQ2pELENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsTUFBTTtRQUM1QixPQUFPO1lBQ0wsUUFBUSxFQUFFLE9BQU87WUFDakIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsWUFBWTtZQUMvQyxPQUFPLEVBQUU7Z0JBQ1AsUUFBUSxFQUFFLFVBQVU7YUFDckI7WUFDRCxRQUFRLEVBQUU7Z0JBQ1IsU0FBUyxFQUFFLE1BQU0sQ0FBQyxRQUFRO2dCQUMxQixZQUFZLEVBQUUsTUFBTSxDQUFDLFFBQVE7YUFDOUI7WUFDRCxRQUFRLEVBQUUsSUFBSTtZQUNkLFNBQVM7Ozs7O1lBQUUsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQUU7Z0JBQzNCLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7b0JBQzVCLFNBQVMsRUFBRTt3QkFDVCxjQUFjLEVBQUU7NEJBQ2Q7Z0NBQ0UsUUFBUSxFQUFFO29DQUNSLE9BQU8sRUFBRSxNQUFNLENBQUMsTUFBTTtvQ0FDdEIsVUFBVSxFQUFFLE1BQU0sQ0FBQyxRQUFRO2lDQUM1QjtnQ0FDRCxhQUFhLEVBQUUsTUFBTSxDQUFDLFdBQVcsSUFBSSxFQUFFOzZCQUN4Qzt5QkFDRjtxQkFDRjtpQkFDRixDQUFDLENBQUM7WUFDTCxDQUFDLENBQUE7WUFDRCxXQUFXOzs7OztZQUFFLENBQUMsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUFFO2dCQUM3QixPQUFPLE9BQU8sQ0FBQyxPQUFPO3FCQUNuQixPQUFPLEVBQUU7cUJBQ1QsSUFBSTs7OztnQkFBQyxPQUFPLENBQUMsRUFBRTs7d0JBQ1YsWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNOzt3QkFDNUIsVUFBVSxHQUFHLENBQUM7O3dCQUNkLFVBQVUsR0FBRyxNQUFNLENBQUMsTUFBTTtvQkFFOUIsc0RBQXNEO29CQUN0RCxJQUFJLE9BQU8sQ0FBQyxNQUFNLElBQUksTUFBTSxJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksVUFBVSxFQUFFO3dCQUMzRCxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ2pCO3lCQUFNO3dCQUNMLE9BQU8sQ0FBQzs0QkFDTixZQUFZLEVBQUUsWUFBWTs0QkFDMUIsVUFBVSxFQUFFLFVBQVU7NEJBQ3RCLFVBQVUsRUFBRSxVQUFVOzRCQUN0QixRQUFRLEVBQUUsTUFBTSxDQUFDLFFBQVE7NEJBQ3pCLE9BQU8sRUFBRSxPQUFPLENBQUMsRUFBRTt5QkFDcEIsQ0FBQyxDQUFDO3dCQUVILElBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBQzs0QkFDdEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt5QkFDdkI7cUJBQ0Y7Z0JBQ0gsQ0FBQyxFQUFDO3FCQUNELEtBQUs7Ozs7Z0JBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDZixNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2YsQ0FBQyxFQUFDLENBQUM7WUFDUCxDQUFDLENBQUE7U0FDRixDQUFDO0lBQ0osQ0FBQzs7OztJQUVELFlBQVk7UUFDVixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTs7Z0JBQ2pDLGdCQUFnQixHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDO1lBQ3ZELGdCQUFnQixDQUFDLEdBQUcsR0FBRywrQ0FBK0MsQ0FBQztZQUN2RSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO1lBQ2xDLGdCQUFnQixDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7WUFDbEMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUM5QyxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLE1BQVc7UUFDcEIsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7O2dCQUNqQyxZQUFZLEdBQVEsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Z0JBQzlDLFVBQVUsR0FBUSxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLFlBQVksR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOztnQkFDL0QsVUFBVSxHQUFRLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDNUQsT0FBTyxDQUFDO2dCQUNOLE9BQU8sRUFBRSxLQUFLLEdBQUcsSUFBSSxFQUFFO2dCQUN2QixZQUFZLEVBQUUsWUFBWTtnQkFDMUIsVUFBVSxFQUFFLFVBQVU7Z0JBQ3RCLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixRQUFRLEVBQUUsS0FBSzthQUNoQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQVc7O1lBQ1osSUFBSSxHQUFRLElBQUk7UUFDcEIsTUFBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztRQUNuQyxPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNyQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDbkIsT0FBTyxDQUFDLEtBQUssQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO2dCQUN4RCxPQUFPLEtBQUssQ0FBQzthQUNkOztrQkFFSyxZQUFZLEdBQVE7Z0JBQ3hCLFNBQVMsRUFBRSxJQUFJO2dCQUNmLFdBQVcsRUFBRSxrQkFBa0I7YUFDaEM7WUFFRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxvQkFBb0IsRUFBRSxZQUFZLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNuRyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxTQUFTOzs7O1lBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLE1BQU0sRUFBRSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1lBQzNFLENBQUMsRUFBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsU0FBUzs7OztZQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7Z0JBQ3pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFBO2dCQUM3QixNQUFNLEVBQUUsQ0FBQztZQUNYLENBQUMsRUFBQyxDQUFDO1lBRUgsTUFBTSxDQUFDLFVBQVU7OztZQUFDLEdBQUcsRUFBRTtnQkFDckIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUN6QixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLEVBQ3BDLDJCQUEyQixDQUM1QixDQUFDO1lBQ0osQ0FBQyxHQUFFLElBQUksQ0FBQyxDQUFDO1FBQ1gsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7WUF6SUYsVUFBVTs7OztZQUpGLG9CQUFvQjs7OztJQU0zQixrQ0FBMkI7O0lBQzNCLGtDQUEyQjs7SUFDM0Isb0NBQTRCOztJQUM1QixnQ0FBYTs7Ozs7SUFDYixpQ0FBc0I7Ozs7O0lBRVYscUNBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7djQgYXMgdXVpZH0gZnJvbSAndXVpZCc7XG5pbXBvcnQge1BheXBhbE1vZGFsQ29tcG9uZW50fSBmcm9tICcuL3BheXBhbC5jb21wb25lbnQnO1xuaW1wb3J0IHsgTmd4U21hcnRNb2RhbFNlcnZpY2UgfSBmcm9tICduZ3gtc21hcnQtbW9kYWwnO1xuXG5pbXBvcnQgJ3VybC1zZWFyY2gtcGFyYW1zLXBvbHlmaWxsJztcbmRlY2xhcmUgdmFyIHdpbmRvdzogYW55O1xuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFBheVBhbFNlcnZpY2Uge1xuICBpc0FsbG93ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgYWRkU2NyaXB0OiBib29sZWFuID0gZmFsc2U7XG4gIHNjcmlwdFJlYWR5OiBib29sZWFuID0gdHJ1ZTtcbiAgaGFuZGxlcjogYW55O1xuICBwcml2YXRlIG1vZGFsUmVmOiBhbnk7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBtb2RhbFNlcnZpY2U6IE5neFNtYXJ0TW9kYWxTZXJ2aWNlKSB7XG4gICAgdGhpcy5fX2luaXQoKTtcbiAgfVxuICBfX2luaXQoKTogdm9pZCB7XG4gICAgaWYgKCF0aGlzLmFkZFNjcmlwdCAmJiB0aGlzLmlzQWxsb3dlZCkge1xuICAgICAgdGhpcy5pbmplY3RTY3JpcHQoKS50aGVuKCgpID0+IHtcbiAgICAgICAgdGhpcy5zY3JpcHRSZWFkeSA9IGZhbHNlO1xuICAgICAgICBjb25zb2xlLmxvZygnUGF5UGFsIGNoZWNrb3V0LmpzIHNjcmlwdCByZWFkeScpO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgY29uZmlnKHBhcmFtcywgcmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIFwibG9jYWxlXCI6ICdlbl9VUycsXG4gICAgICBcImVudlwiOiBwYXJhbXMuaXNUZXN0ID8gJ3NhbmRib3gnIDogJ3Byb2R1Y3Rpb24nLFxuICAgICAgXCJzdHlsZVwiOiB7XG4gICAgICAgIFwibGF5b3V0XCI6ICd2ZXJ0aWNhbCcsXG4gICAgICB9LFxuICAgICAgXCJjbGllbnRcIjoge1xuICAgICAgICBcInNhbmRib3hcIjogcGFyYW1zLnVzZXJuYW1lLFxuICAgICAgICBcInByb2R1Y3Rpb25cIjogcGFyYW1zLnVzZXJuYW1lLFxuICAgICAgfSxcbiAgICAgIFwiY29tbWl0XCI6IHRydWUsXG4gICAgICBcInBheW1lbnRcIjogKGRhdGEsIGFjdGlvbnMpID0+IHtcbiAgICAgICAgcmV0dXJuIGFjdGlvbnMucGF5bWVudC5jcmVhdGUoe1xuICAgICAgICAgIFwicGF5bWVudFwiOiB7XG4gICAgICAgICAgICBcInRyYW5zYWN0aW9uc1wiOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBcImFtb3VudFwiOiB7XG4gICAgICAgICAgICAgICAgICBcInRvdGFsXCI6IHBhcmFtcy5hbW91bnQsIFxuICAgICAgICAgICAgICAgICAgXCJjdXJyZW5jeVwiOiBwYXJhbXMuY3VycmVuY3lcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIFwiZGVzY3JpcHRpb25cIjogcGFyYW1zLmRlc2NyaXB0aW9uIHx8ICcnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdLFxuICAgICAgICAgIH0sXG4gICAgICAgIH0pO1xuICAgICAgfSxcbiAgICAgIG9uQXV0aG9yaXplOiAoZGF0YSwgYWN0aW9ucykgPT4ge1xuICAgICAgICByZXR1cm4gYWN0aW9ucy5wYXltZW50XG4gICAgICAgICAgLmV4ZWN1dGUoKVxuICAgICAgICAgIC50aGVuKHBheW1lbnQgPT4ge1xuICAgICAgICAgICAgbGV0IHRvdGFsX2Ftb3VudCA9IHBhcmFtcy5hbW91bnQ7XG4gICAgICAgICAgICBsZXQgZmVlX2Ftb3VudCA9IDA7XG4gICAgICAgICAgICBsZXQgbmV0X2Ftb3VudCA9IHBhcmFtcy5hbW91bnQ7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIC8vIGNoZWNrIGlmIHRoZSBQYXlQYWwgdHJhbnNhY3Rpb24gZGlkbid0IHdlbnQgdGhyb3VnaFxuICAgICAgICAgICAgaWYgKHBheW1lbnQuaW50ZW50ICE9ICdzYWxlJyAmJiBwYXltZW50LnN0YXRlICE9ICdhcHByb3ZlZCcpIHtcbiAgICAgICAgICAgICAgcmVqZWN0KHBheW1lbnQpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgcmVzb2x2ZSh7XG4gICAgICAgICAgICAgICAgdG90YWxfYW1vdW50OiB0b3RhbF9hbW91bnQsXG4gICAgICAgICAgICAgICAgZmVlX2Ftb3VudDogZmVlX2Ftb3VudCxcbiAgICAgICAgICAgICAgICBuZXRfYW1vdW50OiBuZXRfYW1vdW50LFxuICAgICAgICAgICAgICAgIGN1cnJlbmN5OiBwYXJhbXMuY3VycmVuY3ksXG4gICAgICAgICAgICAgICAgdHJ4bl9pZDogcGF5bWVudC5pZCxcbiAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgaWYodGhpcy5tb2RhbFJlZiAmJiB0aGlzLm1vZGFsUmVmLmNsb3NlKXtcbiAgICAgICAgICAgICAgICB0aGlzLm1vZGFsUmVmLmNsb3NlKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KVxuICAgICAgICAgIC5jYXRjaChlID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGUpO1xuICAgICAgICAgICAgcmVqZWN0KG51bGwpO1xuICAgICAgICAgIH0pO1xuICAgICAgfSxcbiAgICB9O1xuICB9XG5cbiAgaW5qZWN0U2NyaXB0KCkge1xuICAgIHRoaXMuYWRkU2NyaXB0ID0gdHJ1ZTtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgbGV0IHNjcmlwdHRhZ0VsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcbiAgICAgIHNjcmlwdHRhZ0VsZW1lbnQuc3JjID0gJ2h0dHBzOi8vd3d3LnBheXBhbG9iamVjdHMuY29tL2FwaS9jaGVja291dC5qcyc7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50Lm9ubG9hZCA9IHJlc29sdmU7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50Lm9uZXJyb3IgPSByZWplY3Q7XG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcmlwdHRhZ0VsZW1lbnQpO1xuICAgIH0pO1xuICB9XG5cbiAgZmFrZUNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgdG90YWxfYW1vdW50OiBhbnkgPSBwYXJzZUludChwYXJhbXNbJ2Ftb3VudCddKTtcbiAgICAgIGxldCBmZWVfYW1vdW50OiBhbnkgPSAoKDEwMCAqIDIuOSkgLyB0b3RhbF9hbW91bnQgKyAwLjMpLnRvRml4ZWQoMik7XG4gICAgICBsZXQgbmV0X2Ftb3VudDogYW55ID0gKHRvdGFsX2Ftb3VudCAtIGZlZV9hbW91bnQpLnRvRml4ZWQoMik7XG4gICAgICByZXNvbHZlKHtcbiAgICAgICAgdHJ4bl9pZDogJ2NoXycgKyB1dWlkKCksXG4gICAgICAgIHRvdGFsX2Ftb3VudDogdG90YWxfYW1vdW50LFxuICAgICAgICBmZWVfYW1vdW50OiBmZWVfYW1vdW50LFxuICAgICAgICBuZXRfYW1vdW50OiBuZXRfYW1vdW50LFxuICAgICAgICBjdXJyZW5jeTogJ3VzZCcsXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIGxldCBzZWxmOiBhbnkgPSB0aGlzO1xuICAgIHBhcmFtcy5hbW91bnQgPSBwYXJhbXMuYW1vdW50IHx8IDA7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGlmICghdGhpcy5hZGRTY3JpcHQpIHtcbiAgICAgICAgY29uc29sZS5lcnJvcignUGF5UGFsIGNoZWNrb3V0LmpzIHNjcmlwdCBpcyBub3QgcmVhZHknKTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuXG4gICAgICBjb25zdCBtb2RhbE9wdGlvbnM6IGFueSA9IHtcbiAgICAgICAgZXNjYXBhYmxlOiB0cnVlLFxuICAgICAgICBjdXN0b21DbGFzczogJ3BheXBhbE1vZGFsQ2xhc3MnXG4gICAgICB9O1xuXG4gICAgICB0aGlzLm1vZGFsUmVmID0gdGhpcy5tb2RhbFNlcnZpY2UuY3JlYXRlKCdQYXlwYWxNb2RhbCcsIFBheXBhbE1vZGFsQ29tcG9uZW50LCBtb2RhbE9wdGlvbnMpLm9wZW4oKTtcbiAgICAgIHRoaXMubW9kYWxSZWYub25PcGVuRmluaXNoZWQuc3Vic2NyaWJlKChyZXN1bHQpID0+IHtcbiAgICAgICAgdGhpcy5tb2RhbFNlcnZpY2Uuc2V0TW9kYWxEYXRhKHsgaXNUZXN0OiBwYXJhbXMuaXNUZXN0IH0sICdQYXlwYWxNb2RhbCcpO1xuICAgICAgfSk7XG4gICAgICBcbiAgICAgIHRoaXMubW9kYWxSZWYub25DbG9zZS5zdWJzY3JpYmUoKHJlc3VsdCkgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZygnY2xvc2VkJywgcmVzdWx0KVxuICAgICAgICByZWplY3QoKTtcbiAgICAgIH0pO1xuICAgICAgXG4gICAgICB3aW5kb3cuc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIHdpbmRvdy5wYXlwYWwuQnV0dG9uLnJlbmRlcihcbiAgICAgICAgICBzZWxmLmNvbmZpZyhwYXJhbXMsIHJlc29sdmUsIHJlamVjdCksXG4gICAgICAgICAgJyNwYXlwYWwtYnV0dG9ucy1jb250YWluZXInLFxuICAgICAgICApO1xuICAgICAgfSwgMTAwMCk7XG4gICAgfSk7XG4gIH1cbn0iXX0=