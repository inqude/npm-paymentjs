/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { v4 as uuid } from 'uuid';
import 'url-search-params-polyfill';
export class StripeService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        this.isAllowed = false;
        this.addScript = false;
        this.scriptReady = true;
        //  this.__init();
    }
    /**
     * @return {?}
     */
    __init() {
        if (!this.addScript && this.isAllowed) {
            this.injectScript().then((/**
             * @return {?}
             */
            () => {
                this.scriptReady = false;
                console.log('stripe.js script ready');
            }));
        }
    }
    /**
     * @return {?}
     */
    injectScript() {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.stripe.com/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    fakeCharge(params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let total_amount = parseInt(params["amount"]);
            /** @type {?} */
            let fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            let net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + uuid(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    charge(params) {
        /** @type {?} */
        let self = this;
        params.amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            self.handler = window.StripeCheckout.configure({
                key: params.username,
                locale: 'auto',
                name: params.name || '',
                image: params.image || '',
                billingAddress: false,
                zipCode: false,
                allowRememberMe: false,
                description: params.description || '',
                closed: (/**
                 * @param {?} e
                 * @return {?}
                 */
                function (e) {
                    if (!self.handler.isTokenGenerate) {
                        reject({
                            "code": "stripe_transaction_cancelled"
                        });
                    }
                }),
                token: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": (params.amount / 100),
                        "fee_amount": 0,
                        "net_amount": (params.amount / 100),
                        "currency": params.currency,
                        "trxn_id": token.id
                    });
                    return false;
                })
            });
            self.handler.open(params);
        }));
    }
    /**
     * @return {?}
     */
    onPopstate() {
        this.handler.close();
    }
}
StripeService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
StripeService.ctorParameters = () => [
    { type: HttpClient }
];
StripeService.propDecorators = {
    onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
};
if (false) {
    /** @type {?} */
    StripeService.prototype.isAllowed;
    /** @type {?} */
    StripeService.prototype.addScript;
    /** @type {?} */
    StripeService.prototype.scriptReady;
    /** @type {?} */
    StripeService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    StripeService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RyaXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGF5bWVudGpzLyIsInNvdXJjZXMiOlsidmVuZG9yL3N0cmlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLFVBQVUsRUFBZSxNQUFNLHNCQUFzQixDQUFDO0FBQy9ELE9BQU8sRUFBRSxFQUFFLElBQUksSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sNEJBQTRCLENBQUE7QUFJbkMsTUFBTSxPQUFPLGFBQWE7Ozs7SUFLeEIsWUFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUpwQyxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsZ0JBQVcsR0FBWSxJQUFJLENBQUM7UUFHMUIsa0JBQWtCO0lBQ2pCLENBQUM7Ozs7SUFFSixNQUFNO1FBQ0osSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSTs7O1lBQUMsR0FBRyxFQUFFO2dCQUM1QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztnQkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQ3hDLENBQUMsRUFBQyxDQUFBO1NBQ0g7SUFDSCxDQUFDOzs7O0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFOztnQkFDakMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7WUFDdkQsZ0JBQWdCLENBQUMsR0FBRyxHQUFHLHlDQUF5QyxDQUFDO1lBQ2pFLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7WUFDbEMsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztZQUNsQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzlDLENBQUMsRUFBQyxDQUFBO0lBQ0osQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsTUFBVztRQUNwQixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTs7Z0JBQ2pDLFlBQVksR0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDOztnQkFDN0MsVUFBVSxHQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOztnQkFDakUsVUFBVSxHQUFPLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFFM0QsT0FBTyxDQUFDO2dCQUNOLFNBQVMsRUFBRSxLQUFLLEdBQUcsSUFBSSxFQUFFO2dCQUN6QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsWUFBWSxFQUFFLFVBQVU7Z0JBQ3hCLFlBQVksRUFBRSxVQUFVO2dCQUN4QixVQUFVLEVBQUUsS0FBSzthQUNsQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE1BQVc7O1lBQ1osSUFBSSxHQUFRLElBQUk7UUFDcEIsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1FBQzNDLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQ3JDLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUM7Z0JBQzdDLEdBQUcsRUFBRSxNQUFNLENBQUMsUUFBUTtnQkFDcEIsTUFBTSxFQUFFLE1BQU07Z0JBQ2QsSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJLElBQUksRUFBRTtnQkFDdkIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxLQUFLLElBQUksRUFBRTtnQkFDekIsY0FBYyxFQUFFLEtBQUs7Z0JBQ3JCLE9BQU8sRUFBRSxLQUFLO2dCQUNkLGVBQWUsRUFBRSxLQUFLO2dCQUN0QixXQUFXLEVBQUUsTUFBTSxDQUFDLFdBQVcsSUFBSSxFQUFFO2dCQUNyQyxNQUFNOzs7O2dCQUFFLFVBQVUsQ0FBQztvQkFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxFQUFFO3dCQUNqQyxNQUFNLENBQUM7NEJBQ0wsTUFBTSxFQUFFLDhCQUE4Qjt5QkFDdkMsQ0FBQyxDQUFDO3FCQUNKO2dCQUNILENBQUMsQ0FBQTtnQkFDRCxLQUFLOzs7O2dCQUFFLFVBQVMsS0FBSztvQkFDbkIsT0FBTyxDQUFDO3dCQUNOLGNBQWMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO3dCQUNyQyxZQUFZLEVBQUMsQ0FBQzt3QkFDZCxZQUFZLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQzt3QkFDbkMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxRQUFRO3dCQUMzQixTQUFTLEVBQUUsS0FBSyxDQUFDLEVBQUU7cUJBQ3BCLENBQUMsQ0FBQztvQkFDSCxPQUFPLEtBQUssQ0FBQztnQkFDZixDQUFDLENBQUE7YUFDRixDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7SUFHRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQTtJQUN0QixDQUFDOzs7WUFyRkYsVUFBVTs7OztZQUxGLFVBQVU7Ozt5QkF1RmhCLFlBQVksU0FBQyxpQkFBaUI7Ozs7SUFoRi9CLGtDQUEyQjs7SUFDM0Isa0NBQTJCOztJQUMzQixvQ0FBNEI7O0lBQzVCLGdDQUFhOzs7OztJQUNELDZCQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyB2NCBhcyB1dWlkIH0gZnJvbSAndXVpZCc7XG5pbXBvcnQgJ3VybC1zZWFyY2gtcGFyYW1zLXBvbHlmaWxsJ1xuZGVjbGFyZSB2YXIgd2luZG93OiBhbnk7XG7igItcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBTdHJpcGVTZXJ2aWNlIHtcbiAgaXNBbGxvd2VkOiBib29sZWFuID0gZmFsc2U7XG4gIGFkZFNjcmlwdDogYm9vbGVhbiA9IGZhbHNlO1xuICBzY3JpcHRSZWFkeTogYm9vbGVhbiA9IHRydWU7XG4gIGhhbmRsZXI6IGFueTtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7XG4gICAgLy8gIHRoaXMuX19pbml0KCk7XG4gICAgIH1cbuKAi1xuICBfX2luaXQoKTogdm9pZCB7XG4gICAgaWYgKCF0aGlzLmFkZFNjcmlwdCAmJiB0aGlzLmlzQWxsb3dlZCkge1xuICAgICAgdGhpcy5pbmplY3RTY3JpcHQoKS50aGVuKCgpID0+IHtcbiAgICAgICAgdGhpcy5zY3JpcHRSZWFkeSA9IGZhbHNlO1xuICAgICAgICBjb25zb2xlLmxvZygnc3RyaXBlLmpzIHNjcmlwdCByZWFkeScpO1xuICAgICAgfSlcbiAgICB9XG4gIH1cblxuICBpbmplY3RTY3JpcHQoKSB7XG4gICAgdGhpcy5hZGRTY3JpcHQgPSB0cnVlO1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgc2NyaXB0dGFnRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5zcmMgPSAnaHR0cHM6Ly9jaGVja291dC5zdHJpcGUuY29tL2NoZWNrb3V0LmpzJztcbiAgICAgIHNjcmlwdHRhZ0VsZW1lbnQub25sb2FkID0gcmVzb2x2ZTtcbiAgICAgIHNjcmlwdHRhZ0VsZW1lbnQub25lcnJvciA9IHJlamVjdDtcbiAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoc2NyaXB0dGFnRWxlbWVudCk7XG4gICAgfSlcbiAgfVxuXG4gIGZha2VDaGFyZ2UocGFyYW1zOiBhbnkpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgbGV0IHRvdGFsX2Ftb3VudDphbnkgPSBwYXJzZUludChwYXJhbXNbXCJhbW91bnRcIl0pO1xuICAgICAgbGV0IGZlZV9hbW91bnQ6YW55ID0gKCgoMTAwICogMi45KSAvIHRvdGFsX2Ftb3VudCkgKyAwLjMwKS50b0ZpeGVkKDIpO1xuICAgICAgbGV0IG5ldF9hbW91bnQ6YW55ID0gKHRvdGFsX2Ftb3VudCAtIGZlZV9hbW91bnQpLnRvRml4ZWQoMik7XG7igItcbiAgICAgIHJlc29sdmUoe1xuICAgICAgICBcInRyeG5faWRcIjogXCJjaF9cIiArIHV1aWQoKSxcbiAgICAgICAgXCJ0b3RhbF9hbW91bnRcIjogdG90YWxfYW1vdW50LFxuICAgICAgICBcImZlZV9hbW91bnRcIjogZmVlX2Ftb3VudCxcbiAgICAgICAgXCJuZXRfYW1vdW50XCI6IG5ldF9hbW91bnQsXG4gICAgICAgIFwiY3VycmVuY3lcIjogXCJ1c2RcIlxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cbuKAi1xuICBjaGFyZ2UocGFyYW1zOiBhbnkpIHtcbiAgICBsZXQgc2VsZjogYW55ID0gdGhpcztcbiAgICBwYXJhbXMuYW1vdW50ID0gKHBhcmFtcy5hbW91bnQgfHwgMCkgKiAxMDA7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIHNlbGYuaGFuZGxlciA9IHdpbmRvdy5TdHJpcGVDaGVja291dC5jb25maWd1cmUoe1xuICAgICAgICBrZXk6IHBhcmFtcy51c2VybmFtZSxcbiAgICAgICAgbG9jYWxlOiAnYXV0bycsXG4gICAgICAgIG5hbWU6IHBhcmFtcy5uYW1lIHx8ICcnLFxuICAgICAgICBpbWFnZTogcGFyYW1zLmltYWdlIHx8ICcnLFxuICAgICAgICBiaWxsaW5nQWRkcmVzczogZmFsc2UsXG4gICAgICAgIHppcENvZGU6IGZhbHNlLFxuICAgICAgICBhbGxvd1JlbWVtYmVyTWU6IGZhbHNlLFxuICAgICAgICBkZXNjcmlwdGlvbjogcGFyYW1zLmRlc2NyaXB0aW9uIHx8ICcnLFxuICAgICAgICBjbG9zZWQ6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgaWYgKCFzZWxmLmhhbmRsZXIuaXNUb2tlbkdlbmVyYXRlKSB7XG4gICAgICAgICAgICByZWplY3Qoe1xuICAgICAgICAgICAgICBcImNvZGVcIjogXCJzdHJpcGVfdHJhbnNhY3Rpb25fY2FuY2VsbGVkXCJcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdG9rZW46IGZ1bmN0aW9uKHRva2VuKSB7XG4gICAgICAgICAgcmVzb2x2ZSh7XG4gICAgICAgICAgICBcInRvdGFsX2Ftb3VudFwiOiAocGFyYW1zLmFtb3VudCAvIDEwMCksXG4gICAgICAgICAgICBcImZlZV9hbW91bnRcIjowLFxuICAgICAgICAgICAgXCJuZXRfYW1vdW50XCI6IChwYXJhbXMuYW1vdW50IC8gMTAwKSxcbiAgICAgICAgICAgIFwiY3VycmVuY3lcIjogcGFyYW1zLmN1cnJlbmN5LFxuICAgICAgICAgICAgXCJ0cnhuX2lkXCI6IHRva2VuLmlkXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlOyAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgfSk7XG7igItcbiAgICAgIHNlbGYuaGFuZGxlci5vcGVuKHBhcmFtcyk7XG4gICAgfSk7XG4gIH1cbuKAi1xuICBASG9zdExpc3RlbmVyKCd3aW5kb3c6cG9wc3RhdGUnKVxuICBvblBvcHN0YXRlKCkge1xuICAgIHRoaXMuaGFuZGxlci5jbG9zZSgpXG4gIH1cbn0iXX0=