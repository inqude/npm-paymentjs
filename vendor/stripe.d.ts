import { HttpClient } from '@angular/common/http';
import 'url-search-params-polyfill';
export declare class StripeService {
    private http;
    isAllowed: boolean;
    addScript: boolean;
    scriptReady: boolean;
    handler: any;
    constructor(http: HttpClient);
    __init(): void;
    injectScript(): Promise<unknown>;
    fakeCharge(params: any): Promise<unknown>;
    charge(params: any): Promise<unknown>;
    onPopstate(): void;
}
