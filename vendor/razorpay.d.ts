import 'url-search-params-polyfill';
export declare class RazorpayService {
    isAllowed: boolean;
    addScript: boolean;
    scriptReady: boolean;
    handler: any;
    constructor();
    __init(): void;
    injectScript(): Promise<unknown>;
    fakeCharge(params: any): Promise<unknown>;
    charge(params: any): Promise<unknown>;
    onPopstate(): void;
}
