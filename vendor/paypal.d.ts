import { NgxSmartModalService } from 'ngx-smart-modal';
import 'url-search-params-polyfill';
export declare class PayPalService {
    private modalService;
    isAllowed: boolean;
    addScript: boolean;
    scriptReady: boolean;
    handler: any;
    private modalRef;
    constructor(modalService: NgxSmartModalService);
    __init(): void;
    config(params: any, resolve: any, reject: any): {
        "locale": string;
        "env": string;
        "style": {
            "layout": string;
        };
        "client": {
            "sandbox": any;
            "production": any;
        };
        "commit": boolean;
        "payment": (data: any, actions: any) => any;
        onAuthorize: (data: any, actions: any) => any;
    };
    injectScript(): Promise<unknown>;
    fakeCharge(params: any): Promise<unknown>;
    charge(params: any): Promise<unknown>;
}
