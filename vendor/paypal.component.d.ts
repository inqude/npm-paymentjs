import { OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
export declare class PaypalModalComponent implements OnInit {
    modalService: NgxSmartModalService;
    data: any;
    constructor(modalService: NgxSmartModalService);
    ngOnInit(): void;
}
