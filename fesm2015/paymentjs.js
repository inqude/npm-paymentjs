import { __awaiter } from 'tslib';
import { Injectable, HostListener, Component, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { v4 } from 'uuid';
import 'url-search-params-polyfill';
import { NgxSmartModalService, NgxSmartModalModule } from 'ngx-smart-modal';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class StripeService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        this.isAllowed = false;
        this.addScript = false;
        this.scriptReady = true;
        //  this.__init();
    }
    /**
     * @return {?}
     */
    __init() {
        if (!this.addScript && this.isAllowed) {
            this.injectScript().then((/**
             * @return {?}
             */
            () => {
                this.scriptReady = false;
                console.log('stripe.js script ready');
            }));
        }
    }
    /**
     * @return {?}
     */
    injectScript() {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.stripe.com/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    fakeCharge(params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let total_amount = parseInt(params["amount"]);
            /** @type {?} */
            let fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            let net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + v4(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    charge(params) {
        /** @type {?} */
        let self = this;
        params.amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            self.handler = window.StripeCheckout.configure({
                key: params.username,
                locale: 'auto',
                name: params.name || '',
                image: params.image || '',
                billingAddress: false,
                zipCode: false,
                allowRememberMe: false,
                description: params.description || '',
                closed: (/**
                 * @param {?} e
                 * @return {?}
                 */
                function (e) {
                    if (!self.handler.isTokenGenerate) {
                        reject({
                            "code": "stripe_transaction_cancelled"
                        });
                    }
                }),
                token: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": (params.amount / 100),
                        "fee_amount": 0,
                        "net_amount": (params.amount / 100),
                        "currency": params.currency,
                        "trxn_id": token.id
                    });
                    return false;
                })
            });
            self.handler.open(params);
        }));
    }
    /**
     * @return {?}
     */
    onPopstate() {
        this.handler.close();
    }
}
StripeService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
StripeService.ctorParameters = () => [
    { type: HttpClient }
];
StripeService.propDecorators = {
    onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
};
if (false) {
    /** @type {?} */
    StripeService.prototype.isAllowed;
    /** @type {?} */
    StripeService.prototype.addScript;
    /** @type {?} */
    StripeService.prototype.scriptReady;
    /** @type {?} */
    StripeService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    StripeService.prototype.http;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RazorpayService {
    constructor() {
        this.isAllowed = false;
        this.addScript = false;
        this.scriptReady = true;
        //  this.__init();
    }
    /**
     * @return {?}
     */
    __init() {
        if (!this.addScript && this.isAllowed) {
            this.injectScript().then((/**
             * @return {?}
             */
            () => {
                this.scriptReady = false;
                console.log('razorpay.js script ready');
            }));
        }
    }
    /**
     * @return {?}
     */
    injectScript() {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.razorpay.com/v1/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    fakeCharge(params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let total_amount = parseInt(params["amount"]);
            /** @type {?} */
            let fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            let net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + v4(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    charge(params) {
        /** @type {?} */
        let self = this;
        /** @type {?} */
        const amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let options = {
                "key": params.username,
                "amount": amount,
                "currency": params.currency || "INR",
                "name": params.name || '',
                "description": params.description,
                "image": params.image || '',
                "prefill": {
                    "email": params.email || '',
                    "contact": params.phonenumber || ''
                },
                "modal": {
                    "ondismiss": (/**
                     * @return {?}
                     */
                    function () {
                        reject('Razorpay modal closed');
                    })
                },
                handler: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": params.amount,
                        "fee_amount": 0,
                        "net_amount": params.amount,
                        "currency": params.currency,
                        "trxn_id": token.razorpay_payment_id,
                        "raw_response": token
                    });
                    return false;
                })
            }
            // optional
            ;
            // optional
            if (params.order_id) {
                options.order_id = params.order_id;
            }
            self.handler = new window.Razorpay(options);
            self.handler.open();
        }));
    }
    /**
     * @return {?}
     */
    onPopstate() {
        this.handler.close();
    }
}
RazorpayService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
RazorpayService.ctorParameters = () => [];
RazorpayService.propDecorators = {
    onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
};
if (false) {
    /** @type {?} */
    RazorpayService.prototype.isAllowed;
    /** @type {?} */
    RazorpayService.prototype.addScript;
    /** @type {?} */
    RazorpayService.prototype.scriptReady;
    /** @type {?} */
    RazorpayService.prototype.handler;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PaypalModalComponent {
    /**
     * @param {?} modalService
     */
    constructor(modalService) {
        this.modalService = modalService;
        this.data = {};
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const modalRef = this.modalService.getTopOpenedModal();
        modalRef.onDataAdded.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        (data) => {
            this.data = data;
        }));
    }
}
PaypalModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'paypal-modal-component',
                template: "<div *ngIf=\"data.isTest\" style=\"position: fixed;left: 0;top: -23px;padding: 2px 10px;background-color: #3F51B5;color: white;font-size: 0.9rem;border-top-left-radius: 5px;border-top-right-radius: 5px;\">PayPal: Test mode</div>\n<h4 class=\"modal-title\" id=\"modal-basic-title\">PayPal Payment</h4>\n<div class=\"modal-body\" style=\"overflow: auto;\">\n  <div id=\"paypal-buttons-container\" style=\"max-height: 400px;\"></div>\n</div>\n",
                styles: [".paypalModalClass { width: 360px; }"]
            }] }
];
/** @nocollapse */
PaypalModalComponent.ctorParameters = () => [
    { type: NgxSmartModalService }
];
if (false) {
    /** @type {?} */
    PaypalModalComponent.prototype.data;
    /** @type {?} */
    PaypalModalComponent.prototype.modalService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PayPalService {
    /**
     * @param {?} modalService
     */
    constructor(modalService) {
        this.modalService = modalService;
        this.isAllowed = false;
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    __init() {
        if (!this.addScript && this.isAllowed) {
            this.injectScript().then((/**
             * @return {?}
             */
            () => {
                this.scriptReady = false;
                console.log('PayPal checkout.js script ready');
            }));
        }
    }
    /**
     * @param {?} params
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    config(params, resolve, reject) {
        return {
            "locale": 'en_US',
            "env": params.isTest ? 'sandbox' : 'production',
            "style": {
                "layout": 'vertical',
            },
            "client": {
                "sandbox": params.username,
                "production": params.username,
            },
            "commit": true,
            "payment": (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            (data, actions) => {
                return actions.payment.create({
                    "payment": {
                        "transactions": [
                            {
                                "amount": {
                                    "total": params.amount,
                                    "currency": params.currency
                                },
                                "description": params.description || ''
                            },
                        ],
                    },
                });
            }),
            onAuthorize: (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            (data, actions) => {
                return actions.payment
                    .execute()
                    .then((/**
                 * @param {?} payment
                 * @return {?}
                 */
                payment => {
                    /** @type {?} */
                    let total_amount = params.amount;
                    /** @type {?} */
                    let fee_amount = 0;
                    /** @type {?} */
                    let net_amount = params.amount;
                    // check if the PayPal transaction didn't went through
                    if (payment.intent != 'sale' && payment.state != 'approved') {
                        reject(payment);
                    }
                    else {
                        resolve({
                            total_amount: total_amount,
                            fee_amount: fee_amount,
                            net_amount: net_amount,
                            currency: params.currency,
                            trxn_id: payment.id,
                        });
                        if (this.modalRef && this.modalRef.close) {
                            this.modalRef.close();
                        }
                    }
                }))
                    .catch((/**
                 * @param {?} e
                 * @return {?}
                 */
                e => {
                    console.log(e);
                    reject(null);
                }));
            }),
        };
    }
    /**
     * @return {?}
     */
    injectScript() {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    fakeCharge(params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            /** @type {?} */
            let total_amount = parseInt(params['amount']);
            /** @type {?} */
            let fee_amount = ((100 * 2.9) / total_amount + 0.3).toFixed(2);
            /** @type {?} */
            let net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                trxn_id: 'ch_' + v4(),
                total_amount: total_amount,
                fee_amount: fee_amount,
                net_amount: net_amount,
                currency: 'usd',
            });
        }));
    }
    /**
     * @param {?} params
     * @return {?}
     */
    charge(params) {
        /** @type {?} */
        let self = this;
        params.amount = params.amount || 0;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            if (!this.addScript) {
                console.error('PayPal checkout.js script is not ready');
                return false;
            }
            /** @type {?} */
            const modalOptions = {
                escapable: true,
                customClass: 'paypalModalClass'
            };
            this.modalRef = this.modalService.create('PaypalModal', PaypalModalComponent, modalOptions).open();
            this.modalRef.onOpenFinished.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                this.modalService.setModalData({ isTest: params.isTest }, 'PaypalModal');
            }));
            this.modalRef.onClose.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                console.log('closed', result);
                reject();
            }));
            window.setTimeout((/**
             * @return {?}
             */
            () => {
                window.paypal.Button.render(self.config(params, resolve, reject), '#paypal-buttons-container');
            }), 1000);
        }));
    }
}
PayPalService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
PayPalService.ctorParameters = () => [
    { type: NgxSmartModalService }
];
if (false) {
    /** @type {?} */
    PayPalService.prototype.isAllowed;
    /** @type {?} */
    PayPalService.prototype.addScript;
    /** @type {?} */
    PayPalService.prototype.scriptReady;
    /** @type {?} */
    PayPalService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalRef;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalService;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PaymentjsService {
    /**
     * @param {?} stripe
     * @param {?} razorpay
     * @param {?} paypal
     */
    constructor(stripe, razorpay, paypal) {
        this.stripe = stripe;
        this.razorpay = razorpay;
        this.paypal = paypal;
    }
    /**
     * @param {?} className
     * @return {?}
     */
    initialize(className) {
        /** @type {?} */
        const paymentIntrument = (className || '').toLowerCase().split('_')[1];
        this[paymentIntrument].isAllowed = true;
        this[paymentIntrument].__init();
    }
    /**
     * @param {?} data
     * @return {?}
     */
    checkout(data) {
        /** @type {?} */
        let result;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const payment_type = data.paymentInstrument.toLowerCase();
            try {
                switch (payment_type) {
                    case 'payment_stripe':
                        result = yield this.stripe.charge(data.params);
                        break;
                    case 'payment_razorpay':
                        result = yield this.razorpay.charge(data.params);
                        break;
                    case 'payment_paypal':
                        result = yield this.paypal.charge(data.params);
                        break;
                    default:
                        reject(`Unsupported Payment Instrument ${data.paymentInstrument}`);
                        return false;
                        break;
                }
                result["isTest"] = data.params.isTest;
                result["paymentInstrument"] = data.paymentInstrument;
                resolve(result);
            }
            catch (e) {
                console.log(e);
                reject('Transaction cancelled!');
            }
        })));
    }
}
PaymentjsService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
PaymentjsService.ctorParameters = () => [
    { type: StripeService },
    { type: RazorpayService },
    { type: PayPalService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.stripe;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.razorpay;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.paypal;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PaymentjsComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
}
PaymentjsComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-paymentjs',
                template: `
    <p>
      paymentjs works!
    </p>
  `
            }] }
];
/** @nocollapse */
PaymentjsComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PaymentjsModule {
}
PaymentjsModule.decorators = [
    { type: NgModule, args: [{
                declarations: [PaymentjsComponent, PaypalModalComponent],
                imports: [CommonModule, NgxSmartModalModule.forRoot()],
                entryComponents: [PaypalModalComponent],
                providers: [PaymentjsService, StripeService, RazorpayService, PayPalService],
                exports: [PaymentjsComponent, PaypalModalComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { PaymentjsComponent, PaymentjsModule, PaymentjsService, StripeService as ɵa, RazorpayService as ɵb, PayPalService as ɵc, PaypalModalComponent as ɵd };
//# sourceMappingURL=paymentjs.js.map
