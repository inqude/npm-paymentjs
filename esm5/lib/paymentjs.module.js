/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { PaymentjsComponent } from './paymentjs.component';
import { PaymentjsService } from './paymentjs.service';
import { StripeService } from '../vendor/stripe';
import { RazorpayService } from '../vendor/razorpay';
import { PayPalService } from '../vendor/paypal';
import { PaypalModalComponent } from '../vendor/paypal.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { CommonModule } from '@angular/common';
var PaymentjsModule = /** @class */ (function () {
    function PaymentjsModule() {
    }
    PaymentjsModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [PaymentjsComponent, PaypalModalComponent],
                    imports: [CommonModule, NgxSmartModalModule.forRoot()],
                    entryComponents: [PaypalModalComponent],
                    providers: [PaymentjsService, StripeService, RazorpayService, PayPalService],
                    exports: [PaymentjsComponent, PaypalModalComponent]
                },] }
    ];
    return PaymentjsModule;
}());
export { PaymentjsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5bWVudGpzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BheW1lbnRqcy8iLCJzb3VyY2VzIjpbImxpYi9wYXltZW50anMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzNELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQztJQUFBO0lBTytCLENBQUM7O2dCQVAvQixRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsa0JBQWtCLEVBQUUsb0JBQW9CLENBQUM7b0JBQ3hELE9BQU8sRUFBRSxDQUFDLFlBQVksRUFBRSxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDdEQsZUFBZSxFQUFFLENBQUMsb0JBQW9CLENBQUM7b0JBQ3ZDLFNBQVMsRUFBRSxDQUFDLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxlQUFlLEVBQUUsYUFBYSxDQUFDO29CQUM1RSxPQUFPLEVBQUUsQ0FBQyxrQkFBa0IsRUFBRSxvQkFBb0IsQ0FBQztpQkFDcEQ7O0lBQzhCLHNCQUFDO0NBQUEsQUFQaEMsSUFPZ0M7U0FBbkIsZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQYXltZW50anNDb21wb25lbnQgfSBmcm9tICcuL3BheW1lbnRqcy5jb21wb25lbnQnO1xuaW1wb3J0IHsgUGF5bWVudGpzU2VydmljZSB9IGZyb20gJy4vcGF5bWVudGpzLnNlcnZpY2UnO1xuaW1wb3J0IHsgU3RyaXBlU2VydmljZSB9IGZyb20gJy4uL3ZlbmRvci9zdHJpcGUnO1xuaW1wb3J0IHsgUmF6b3JwYXlTZXJ2aWNlIH0gZnJvbSAnLi4vdmVuZG9yL3Jhem9ycGF5JztcbmltcG9ydCB7IFBheVBhbFNlcnZpY2UgfSBmcm9tICcuLi92ZW5kb3IvcGF5cGFsJztcbmltcG9ydCB7IFBheXBhbE1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vdmVuZG9yL3BheXBhbC5jb21wb25lbnQnO1xuaW1wb3J0IHsgTmd4U21hcnRNb2RhbE1vZHVsZSB9IGZyb20gJ25neC1zbWFydC1tb2RhbCc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtQYXltZW50anNDb21wb25lbnQsIFBheXBhbE1vZGFsQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgTmd4U21hcnRNb2RhbE1vZHVsZS5mb3JSb290KCldLFxuICBlbnRyeUNvbXBvbmVudHM6IFtQYXlwYWxNb2RhbENvbXBvbmVudF0sXG4gIHByb3ZpZGVyczogW1BheW1lbnRqc1NlcnZpY2UsIFN0cmlwZVNlcnZpY2UsIFJhem9ycGF5U2VydmljZSwgUGF5UGFsU2VydmljZV0sXG4gIGV4cG9ydHM6IFtQYXltZW50anNDb21wb25lbnQsIFBheXBhbE1vZGFsQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBQYXltZW50anNNb2R1bGUgeyB9XG4iXX0=