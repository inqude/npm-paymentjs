/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { StripeService } from '../vendor/stripe';
import { RazorpayService } from '../vendor/razorpay';
import { PayPalService } from '../vendor/paypal';
var PaymentjsService = /** @class */ (function () {
    function PaymentjsService(stripe, razorpay, paypal) {
        this.stripe = stripe;
        this.razorpay = razorpay;
        this.paypal = paypal;
    }
    /**
     * @param {?} className
     * @return {?}
     */
    PaymentjsService.prototype.initialize = /**
     * @param {?} className
     * @return {?}
     */
    function (className) {
        /** @type {?} */
        var paymentIntrument = (className || '').toLowerCase().split('_')[1];
        this[paymentIntrument].isAllowed = true;
        this[paymentIntrument].__init();
    };
    /**
     * @param {?} data
     * @return {?}
     */
    PaymentjsService.prototype.checkout = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        var _this = this;
        /** @type {?} */
        var result;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var payment_type, _a, e_1;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        payment_type = data.paymentInstrument.toLowerCase();
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 10, , 11]);
                        _a = payment_type;
                        switch (_a) {
                            case 'payment_stripe': return [3 /*break*/, 2];
                            case 'payment_razorpay': return [3 /*break*/, 4];
                            case 'payment_paypal': return [3 /*break*/, 6];
                        }
                        return [3 /*break*/, 8];
                    case 2: return [4 /*yield*/, this.stripe.charge(data.params)];
                    case 3:
                        result = _b.sent();
                        return [3 /*break*/, 9];
                    case 4: return [4 /*yield*/, this.razorpay.charge(data.params)];
                    case 5:
                        result = _b.sent();
                        return [3 /*break*/, 9];
                    case 6: return [4 /*yield*/, this.paypal.charge(data.params)];
                    case 7:
                        result = _b.sent();
                        return [3 /*break*/, 9];
                    case 8:
                        reject("Unsupported Payment Instrument " + data.paymentInstrument);
                        return [2 /*return*/, false];
                    case 9:
                        result["isTest"] = data.params.isTest;
                        result["paymentInstrument"] = data.paymentInstrument;
                        resolve(result);
                        return [3 /*break*/, 11];
                    case 10:
                        e_1 = _b.sent();
                        console.log(e_1);
                        reject('Transaction cancelled!');
                        return [3 /*break*/, 11];
                    case 11: return [2 /*return*/];
                }
            });
        }); }));
    };
    PaymentjsService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    PaymentjsService.ctorParameters = function () { return [
        { type: StripeService },
        { type: RazorpayService },
        { type: PayPalService }
    ]; };
    return PaymentjsService;
}());
export { PaymentjsService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.stripe;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.razorpay;
    /**
     * @type {?}
     * @private
     */
    PaymentjsService.prototype.paypal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5bWVudGpzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYXltZW50anMvIiwic291cmNlcyI6WyJsaWIvcGF5bWVudGpzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRWpEO0lBRUUsMEJBQ1UsTUFBcUIsRUFDckIsUUFBeUIsRUFDekIsTUFBcUI7UUFGckIsV0FBTSxHQUFOLE1BQU0sQ0FBZTtRQUNyQixhQUFRLEdBQVIsUUFBUSxDQUFpQjtRQUN6QixXQUFNLEdBQU4sTUFBTSxDQUFlO0lBQzNCLENBQUM7Ozs7O0lBRUwscUNBQVU7Ozs7SUFBVixVQUFXLFNBQWlCOztZQUNwQixnQkFBZ0IsR0FBVyxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzlFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDeEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDbEMsQ0FBQzs7Ozs7SUFFRCxtQ0FBUTs7OztJQUFSLFVBQVMsSUFBUztRQUFsQixpQkErQkM7O1lBOUJLLE1BQVc7UUFDZixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFNLE9BQU8sRUFBRSxNQUFNOzs7Ozt3QkFDaEMsWUFBWSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUU7Ozs7d0JBRS9DLEtBQUEsWUFBWSxDQUFBOztpQ0FDYixnQkFBZ0IsQ0FBQyxDQUFqQix3QkFBZ0I7aUNBSWhCLGtCQUFrQixDQUFDLENBQW5CLHdCQUFrQjtpQ0FJbEIsZ0JBQWdCLENBQUMsQ0FBakIsd0JBQWdCOzs7NEJBUFYscUJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBOUMsTUFBTSxHQUFHLFNBQXFDLENBQUM7d0JBQy9DLHdCQUFNOzRCQUdHLHFCQUFNLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQWhELE1BQU0sR0FBRyxTQUF1QyxDQUFDO3dCQUNqRCx3QkFBTTs0QkFHRyxxQkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUE5QyxNQUFNLEdBQUcsU0FBcUMsQ0FBQzt3QkFDL0Msd0JBQU07O3dCQUdOLE1BQU0sQ0FBQyxvQ0FBa0MsSUFBSSxDQUFDLGlCQUFtQixDQUFDLENBQUM7d0JBQ25FLHNCQUFPLEtBQUssRUFBQzs7d0JBR2pCLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQzt3QkFDdEMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO3dCQUNyRCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7Ozs7d0JBRWhCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBQyxDQUFDLENBQUM7d0JBQ2YsTUFBTSxDQUFDLHdCQUF3QixDQUFDLENBQUM7Ozs7O2FBRXBDLEVBQUMsQ0FBQTtJQUNKLENBQUM7O2dCQTdDRixVQUFVOzs7O2dCQUpGLGFBQWE7Z0JBQ2IsZUFBZTtnQkFDZixhQUFhOztJQWdEdEIsdUJBQUM7Q0FBQSxBQTlDRCxJQThDQztTQTdDWSxnQkFBZ0I7Ozs7OztJQUV6QixrQ0FBNkI7Ozs7O0lBQzdCLG9DQUFpQzs7Ozs7SUFDakMsa0NBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU3RyaXBlU2VydmljZSB9IGZyb20gJy4uL3ZlbmRvci9zdHJpcGUnO1xuaW1wb3J0IHsgUmF6b3JwYXlTZXJ2aWNlIH0gZnJvbSAnLi4vdmVuZG9yL3Jhem9ycGF5JztcbmltcG9ydCB7IFBheVBhbFNlcnZpY2UgfSBmcm9tICcuLi92ZW5kb3IvcGF5cGFsJztcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFBheW1lbnRqc1NlcnZpY2Uge1xuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIHN0cmlwZTogU3RyaXBlU2VydmljZSxcbiAgICBwcml2YXRlIHJhem9ycGF5OiBSYXpvcnBheVNlcnZpY2UsXG4gICAgcHJpdmF0ZSBwYXlwYWw6IFBheVBhbFNlcnZpY2UsXG4gICkgeyB9XG4gIFxuICBpbml0aWFsaXplKGNsYXNzTmFtZTogc3RyaW5nKXtcbiAgICBjb25zdCBwYXltZW50SW50cnVtZW50OiBzdHJpbmcgPSAoY2xhc3NOYW1lIHx8ICcnKS50b0xvd2VyQ2FzZSgpLnNwbGl0KCdfJylbMV07XG4gICAgdGhpc1twYXltZW50SW50cnVtZW50XS5pc0FsbG93ZWQgPSB0cnVlO1xuICAgIHRoaXNbcGF5bWVudEludHJ1bWVudF0uX19pbml0KCk7XG4gIH1cblxuICBjaGVja291dChkYXRhOiBhbnkpe1xuICAgIGxldCByZXN1bHQ6IGFueTtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoYXN5bmMocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBjb25zdCBwYXltZW50X3R5cGUgPSBkYXRhLnBheW1lbnRJbnN0cnVtZW50LnRvTG93ZXJDYXNlKCk7XG4gICAgICB0cnkge1xuICAgICAgICBzd2l0Y2ggKHBheW1lbnRfdHlwZSkge1xuICAgICAgICAgIGNhc2UgJ3BheW1lbnRfc3RyaXBlJzpcbiAgICAgICAgICAgIHJlc3VsdCA9IGF3YWl0IHRoaXMuc3RyaXBlLmNoYXJnZShkYXRhLnBhcmFtcyk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgIGNhc2UgJ3BheW1lbnRfcmF6b3JwYXknOlxuICAgICAgICAgICAgcmVzdWx0ID0gYXdhaXQgdGhpcy5yYXpvcnBheS5jaGFyZ2UoZGF0YS5wYXJhbXMpO1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICBjYXNlICdwYXltZW50X3BheXBhbCc6XG4gICAgICAgICAgICByZXN1bHQgPSBhd2FpdCB0aGlzLnBheXBhbC5jaGFyZ2UoZGF0YS5wYXJhbXMpO1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgcmVqZWN0KGBVbnN1cHBvcnRlZCBQYXltZW50IEluc3RydW1lbnQgJHtkYXRhLnBheW1lbnRJbnN0cnVtZW50fWApO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgICAgcmVzdWx0W1wiaXNUZXN0XCJdID0gZGF0YS5wYXJhbXMuaXNUZXN0O1xuICAgICAgICByZXN1bHRbXCJwYXltZW50SW5zdHJ1bWVudFwiXSA9IGRhdGEucGF5bWVudEluc3RydW1lbnQ7XG4gICAgICAgIHJlc29sdmUocmVzdWx0KTtcbiAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgY29uc29sZS5sb2coZSk7XG4gICAgICAgIHJlamVjdCgnVHJhbnNhY3Rpb24gY2FuY2VsbGVkIScpO1xuICAgICAgfVxuICAgIH0pXG4gIH1cbn1cbiJdfQ==