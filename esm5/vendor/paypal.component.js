/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
var PaypalModalComponent = /** @class */ (function () {
    function PaypalModalComponent(modalService) {
        this.modalService = modalService;
        this.data = {};
    }
    /**
     * @return {?}
     */
    PaypalModalComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var modalRef = this.modalService.getTopOpenedModal();
        modalRef.onDataAdded.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            _this.data = data;
        }));
    };
    PaypalModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'paypal-modal-component',
                    template: "<div *ngIf=\"data.isTest\" style=\"position: fixed;left: 0;top: -23px;padding: 2px 10px;background-color: #3F51B5;color: white;font-size: 0.9rem;border-top-left-radius: 5px;border-top-right-radius: 5px;\">PayPal: Test mode</div>\n<h4 class=\"modal-title\" id=\"modal-basic-title\">PayPal Payment</h4>\n<div class=\"modal-body\" style=\"overflow: auto;\">\n  <div id=\"paypal-buttons-container\" style=\"max-height: 400px;\"></div>\n</div>\n",
                    styles: [".paypalModalClass { width: 360px; }"]
                }] }
    ];
    /** @nocollapse */
    PaypalModalComponent.ctorParameters = function () { return [
        { type: NgxSmartModalService }
    ]; };
    return PaypalModalComponent;
}());
export { PaypalModalComponent };
if (false) {
    /** @type {?} */
    PaypalModalComponent.prototype.data;
    /** @type {?} */
    PaypalModalComponent.prototype.modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5cGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BheW1lbnRqcy8iLCJzb3VyY2VzIjpbInZlbmRvci9wYXlwYWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXZEO0lBT0UsOEJBQW1CLFlBQWtDO1FBQWxDLGlCQUFZLEdBQVosWUFBWSxDQUFzQjtRQUQ5QyxTQUFJLEdBQVEsRUFBRSxDQUFDO0lBQ21DLENBQUM7Ozs7SUFFMUQsdUNBQVE7OztJQUFSO1FBQUEsaUJBS0M7O1lBSk8sUUFBUSxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsaUJBQWlCLEVBQUU7UUFDM0QsUUFBUSxDQUFDLFdBQVcsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxJQUFTO1lBQ3ZDLEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ25CLENBQUMsRUFBQyxDQUFBO0lBQ0osQ0FBQzs7Z0JBZEYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSx3QkFBd0I7b0JBQ2xDLG9jQUEwQzs2QkFDakMscUNBQXFDO2lCQUMvQzs7OztnQkFOUSxvQkFBb0I7O0lBaUI3QiwyQkFBQztDQUFBLEFBZkQsSUFlQztTQVZZLG9CQUFvQjs7O0lBQy9CLG9DQUFzQjs7SUFDViw0Q0FBeUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmd4U21hcnRNb2RhbFNlcnZpY2UgfSBmcm9tICduZ3gtc21hcnQtbW9kYWwnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdwYXlwYWwtbW9kYWwtY29tcG9uZW50JyxcbiAgdGVtcGxhdGVVcmw6ICdwYXlwYWwtbW9kYWwuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZXM6IFtcIi5wYXlwYWxNb2RhbENsYXNzIHsgd2lkdGg6IDM2MHB4OyB9XCJdXG59KVxuZXhwb3J0IGNsYXNzIFBheXBhbE1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHsgIFxuICBwdWJsaWMgZGF0YTogYW55ID0ge307XG4gIGNvbnN0cnVjdG9yKHB1YmxpYyBtb2RhbFNlcnZpY2U6IE5neFNtYXJ0TW9kYWxTZXJ2aWNlKSB7IH1cblxuICBuZ09uSW5pdCgpe1xuICAgIGNvbnN0IG1vZGFsUmVmOiBhbnkgPSB0aGlzLm1vZGFsU2VydmljZS5nZXRUb3BPcGVuZWRNb2RhbCgpO1xuICAgIG1vZGFsUmVmLm9uRGF0YUFkZGVkLnN1YnNjcmliZSgoZGF0YTogYW55KSA9PiB7XG4gICAgICB0aGlzLmRhdGEgPSBkYXRhO1xuICAgIH0pICAgIFxuICB9XG59XG4iXX0=