/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { v4 as uuid } from 'uuid';
import 'url-search-params-polyfill';
var StripeService = /** @class */ (function () {
    function StripeService(http) {
        this.http = http;
        this.isAllowed = false;
        this.addScript = false;
        this.scriptReady = true;
        //  this.__init();
    }
    /**
     * @return {?}
     */
    StripeService.prototype.__init = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.addScript && this.isAllowed) {
            this.injectScript().then((/**
             * @return {?}
             */
            function () {
                _this.scriptReady = false;
                console.log('stripe.js script ready');
            }));
        }
    };
    /**
     * @return {?}
     */
    StripeService.prototype.injectScript = /**
     * @return {?}
     */
    function () {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.stripe.com/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    StripeService.prototype.fakeCharge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var total_amount = parseInt(params["amount"]);
            /** @type {?} */
            var fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            var net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + uuid(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    StripeService.prototype.charge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var self = this;
        params.amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            self.handler = window.StripeCheckout.configure({
                key: params.username,
                locale: 'auto',
                name: params.name || '',
                image: params.image || '',
                billingAddress: false,
                zipCode: false,
                allowRememberMe: false,
                description: params.description || '',
                closed: (/**
                 * @param {?} e
                 * @return {?}
                 */
                function (e) {
                    if (!self.handler.isTokenGenerate) {
                        reject({
                            "code": "stripe_transaction_cancelled"
                        });
                    }
                }),
                token: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": (params.amount / 100),
                        "fee_amount": 0,
                        "net_amount": (params.amount / 100),
                        "currency": params.currency,
                        "trxn_id": token.id
                    });
                    return false;
                })
            });
            self.handler.open(params);
        }));
    };
    /**
     * @return {?}
     */
    StripeService.prototype.onPopstate = /**
     * @return {?}
     */
    function () {
        this.handler.close();
    };
    StripeService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    StripeService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    StripeService.propDecorators = {
        onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
    };
    return StripeService;
}());
export { StripeService };
if (false) {
    /** @type {?} */
    StripeService.prototype.isAllowed;
    /** @type {?} */
    StripeService.prototype.addScript;
    /** @type {?} */
    StripeService.prototype.scriptReady;
    /** @type {?} */
    StripeService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    StripeService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RyaXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGF5bWVudGpzLyIsInNvdXJjZXMiOlsidmVuZG9yL3N0cmlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLFVBQVUsRUFBZSxNQUFNLHNCQUFzQixDQUFDO0FBQy9ELE9BQU8sRUFBRSxFQUFFLElBQUksSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sNEJBQTRCLENBQUE7QUFHbkM7SUFNRSx1QkFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUpwQyxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsZ0JBQVcsR0FBWSxJQUFJLENBQUM7UUFHMUIsa0JBQWtCO0lBQ2pCLENBQUM7Ozs7SUFFSiw4QkFBTTs7O0lBQU47UUFBQSxpQkFPQztRQU5DLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDckMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUk7OztZQUFDO2dCQUN2QixLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztnQkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQ3hDLENBQUMsRUFBQyxDQUFBO1NBQ0g7SUFDSCxDQUFDOzs7O0lBRUQsb0NBQVk7OztJQUFaO1FBQ0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTs7Z0JBQzdCLGdCQUFnQixHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDO1lBQ3ZELGdCQUFnQixDQUFDLEdBQUcsR0FBRyx5Q0FBeUMsQ0FBQztZQUNqRSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO1lBQ2xDLGdCQUFnQixDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7WUFDbEMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUM5QyxDQUFDLEVBQUMsQ0FBQTtJQUNKLENBQUM7Ozs7O0lBRUQsa0NBQVU7Ozs7SUFBVixVQUFXLE1BQVc7UUFDcEIsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTs7Z0JBQzdCLFlBQVksR0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDOztnQkFDN0MsVUFBVSxHQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOztnQkFDakUsVUFBVSxHQUFPLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFFM0QsT0FBTyxDQUFDO2dCQUNOLFNBQVMsRUFBRSxLQUFLLEdBQUcsSUFBSSxFQUFFO2dCQUN6QixjQUFjLEVBQUUsWUFBWTtnQkFDNUIsWUFBWSxFQUFFLFVBQVU7Z0JBQ3hCLFlBQVksRUFBRSxVQUFVO2dCQUN4QixVQUFVLEVBQUUsS0FBSzthQUNsQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsOEJBQU07Ozs7SUFBTixVQUFPLE1BQVc7O1lBQ1osSUFBSSxHQUFRLElBQUk7UUFDcEIsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1FBQzNDLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDakMsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQztnQkFDN0MsR0FBRyxFQUFFLE1BQU0sQ0FBQyxRQUFRO2dCQUNwQixNQUFNLEVBQUUsTUFBTTtnQkFDZCxJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUksSUFBSSxFQUFFO2dCQUN2QixLQUFLLEVBQUUsTUFBTSxDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUN6QixjQUFjLEVBQUUsS0FBSztnQkFDckIsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsZUFBZSxFQUFFLEtBQUs7Z0JBQ3RCLFdBQVcsRUFBRSxNQUFNLENBQUMsV0FBVyxJQUFJLEVBQUU7Z0JBQ3JDLE1BQU07Ozs7Z0JBQUUsVUFBVSxDQUFDO29CQUNqQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUU7d0JBQ2pDLE1BQU0sQ0FBQzs0QkFDTCxNQUFNLEVBQUUsOEJBQThCO3lCQUN2QyxDQUFDLENBQUM7cUJBQ0o7Z0JBQ0gsQ0FBQyxDQUFBO2dCQUNELEtBQUs7Ozs7Z0JBQUUsVUFBUyxLQUFLO29CQUNuQixPQUFPLENBQUM7d0JBQ04sY0FBYyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7d0JBQ3JDLFlBQVksRUFBQyxDQUFDO3dCQUNkLFlBQVksRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO3dCQUNuQyxVQUFVLEVBQUUsTUFBTSxDQUFDLFFBQVE7d0JBQzNCLFNBQVMsRUFBRSxLQUFLLENBQUMsRUFBRTtxQkFDcEIsQ0FBQyxDQUFDO29CQUNILE9BQU8sS0FBSyxDQUFDO2dCQUNmLENBQUMsQ0FBQTthQUNGLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzVCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUdELGtDQUFVOzs7SUFEVjtRQUVFLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUE7SUFDdEIsQ0FBQzs7Z0JBckZGLFVBQVU7Ozs7Z0JBTEYsVUFBVTs7OzZCQXVGaEIsWUFBWSxTQUFDLGlCQUFpQjs7SUFJakMsb0JBQUM7Q0FBQSxBQXRGRCxJQXNGQztTQXJGWSxhQUFhOzs7SUFDeEIsa0NBQTJCOztJQUMzQixrQ0FBMkI7O0lBQzNCLG9DQUE0Qjs7SUFDNUIsZ0NBQWE7Ozs7O0lBQ0QsNkJBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSG9zdExpc3RlbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IHY0IGFzIHV1aWQgfSBmcm9tICd1dWlkJztcbmltcG9ydCAndXJsLXNlYXJjaC1wYXJhbXMtcG9seWZpbGwnXG5kZWNsYXJlIHZhciB3aW5kb3c6IGFueTtcbuKAi1xuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFN0cmlwZVNlcnZpY2Uge1xuICBpc0FsbG93ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgYWRkU2NyaXB0OiBib29sZWFuID0gZmFsc2U7XG4gIHNjcmlwdFJlYWR5OiBib29sZWFuID0gdHJ1ZTtcbiAgaGFuZGxlcjogYW55O1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHtcbiAgICAvLyAgdGhpcy5fX2luaXQoKTtcbiAgICAgfVxu4oCLXG4gIF9faW5pdCgpOiB2b2lkIHtcbiAgICBpZiAoIXRoaXMuYWRkU2NyaXB0ICYmIHRoaXMuaXNBbGxvd2VkKSB7XG4gICAgICB0aGlzLmluamVjdFNjcmlwdCgpLnRoZW4oKCkgPT4ge1xuICAgICAgICB0aGlzLnNjcmlwdFJlYWR5ID0gZmFsc2U7XG4gICAgICAgIGNvbnNvbGUubG9nKCdzdHJpcGUuanMgc2NyaXB0IHJlYWR5Jyk7XG4gICAgICB9KVxuICAgIH1cbiAgfVxuXG4gIGluamVjdFNjcmlwdCgpIHtcbiAgICB0aGlzLmFkZFNjcmlwdCA9IHRydWU7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCBzY3JpcHR0YWdFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50LnNyYyA9ICdodHRwczovL2NoZWNrb3V0LnN0cmlwZS5jb20vY2hlY2tvdXQuanMnO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5vbmxvYWQgPSByZXNvbHZlO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5vbmVycm9yID0gcmVqZWN0O1xuICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChzY3JpcHR0YWdFbGVtZW50KTtcbiAgICB9KVxuICB9XG5cbiAgZmFrZUNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgdG90YWxfYW1vdW50OmFueSA9IHBhcnNlSW50KHBhcmFtc1tcImFtb3VudFwiXSk7XG4gICAgICBsZXQgZmVlX2Ftb3VudDphbnkgPSAoKCgxMDAgKiAyLjkpIC8gdG90YWxfYW1vdW50KSArIDAuMzApLnRvRml4ZWQoMik7XG4gICAgICBsZXQgbmV0X2Ftb3VudDphbnkgPSAodG90YWxfYW1vdW50IC0gZmVlX2Ftb3VudCkudG9GaXhlZCgyKTtcbuKAi1xuICAgICAgcmVzb2x2ZSh7XG4gICAgICAgIFwidHJ4bl9pZFwiOiBcImNoX1wiICsgdXVpZCgpLFxuICAgICAgICBcInRvdGFsX2Ftb3VudFwiOiB0b3RhbF9hbW91bnQsXG4gICAgICAgIFwiZmVlX2Ftb3VudFwiOiBmZWVfYW1vdW50LFxuICAgICAgICBcIm5ldF9hbW91bnRcIjogbmV0X2Ftb3VudCxcbiAgICAgICAgXCJjdXJyZW5jeVwiOiBcInVzZFwiXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxu4oCLXG4gIGNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIGxldCBzZWxmOiBhbnkgPSB0aGlzO1xuICAgIHBhcmFtcy5hbW91bnQgPSAocGFyYW1zLmFtb3VudCB8fCAwKSAqIDEwMDtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgc2VsZi5oYW5kbGVyID0gd2luZG93LlN0cmlwZUNoZWNrb3V0LmNvbmZpZ3VyZSh7XG4gICAgICAgIGtleTogcGFyYW1zLnVzZXJuYW1lLFxuICAgICAgICBsb2NhbGU6ICdhdXRvJyxcbiAgICAgICAgbmFtZTogcGFyYW1zLm5hbWUgfHwgJycsXG4gICAgICAgIGltYWdlOiBwYXJhbXMuaW1hZ2UgfHwgJycsXG4gICAgICAgIGJpbGxpbmdBZGRyZXNzOiBmYWxzZSxcbiAgICAgICAgemlwQ29kZTogZmFsc2UsXG4gICAgICAgIGFsbG93UmVtZW1iZXJNZTogZmFsc2UsXG4gICAgICAgIGRlc2NyaXB0aW9uOiBwYXJhbXMuZGVzY3JpcHRpb24gfHwgJycsXG4gICAgICAgIGNsb3NlZDogZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICBpZiAoIXNlbGYuaGFuZGxlci5pc1Rva2VuR2VuZXJhdGUpIHtcbiAgICAgICAgICAgIHJlamVjdCh7XG4gICAgICAgICAgICAgIFwiY29kZVwiOiBcInN0cmlwZV90cmFuc2FjdGlvbl9jYW5jZWxsZWRcIlxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB0b2tlbjogZnVuY3Rpb24odG9rZW4pIHtcbiAgICAgICAgICByZXNvbHZlKHtcbiAgICAgICAgICAgIFwidG90YWxfYW1vdW50XCI6IChwYXJhbXMuYW1vdW50IC8gMTAwKSxcbiAgICAgICAgICAgIFwiZmVlX2Ftb3VudFwiOjAsXG4gICAgICAgICAgICBcIm5ldF9hbW91bnRcIjogKHBhcmFtcy5hbW91bnQgLyAxMDApLFxuICAgICAgICAgICAgXCJjdXJyZW5jeVwiOiBwYXJhbXMuY3VycmVuY3ksXG4gICAgICAgICAgICBcInRyeG5faWRcIjogdG9rZW4uaWRcbiAgICAgICAgICB9KTtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7ICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICB9KTtcbuKAi1xuICAgICAgc2VsZi5oYW5kbGVyLm9wZW4ocGFyYW1zKTtcbiAgICB9KTtcbiAgfVxu4oCLXG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpwb3BzdGF0ZScpXG4gIG9uUG9wc3RhdGUoKSB7XG4gICAgdGhpcy5oYW5kbGVyLmNsb3NlKClcbiAgfVxufSJdfQ==