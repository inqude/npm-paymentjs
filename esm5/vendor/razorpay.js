/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, HostListener } from '@angular/core';
import { v4 as uuid } from 'uuid';
import 'url-search-params-polyfill';
var RazorpayService = /** @class */ (function () {
    function RazorpayService() {
        this.isAllowed = false;
        this.addScript = false;
        this.scriptReady = true;
        //  this.__init();
    }
    /**
     * @return {?}
     */
    RazorpayService.prototype.__init = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.addScript && this.isAllowed) {
            this.injectScript().then((/**
             * @return {?}
             */
            function () {
                _this.scriptReady = false;
                console.log('razorpay.js script ready');
            }));
        }
    };
    /**
     * @return {?}
     */
    RazorpayService.prototype.injectScript = /**
     * @return {?}
     */
    function () {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://checkout.razorpay.com/v1/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    RazorpayService.prototype.fakeCharge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var total_amount = parseInt(params["amount"]);
            /** @type {?} */
            var fee_amount = (((100 * 2.9) / total_amount) + 0.30).toFixed(2);
            /** @type {?} */
            var net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                "trxn_id": "ch_" + uuid(),
                "total_amount": total_amount,
                "fee_amount": fee_amount,
                "net_amount": net_amount,
                "currency": "usd"
            });
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    RazorpayService.prototype.charge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        /** @type {?} */
        var self = this;
        /** @type {?} */
        var amount = (params.amount || 0) * 100;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var options = {
                "key": params.username,
                "amount": amount,
                "currency": params.currency || "INR",
                "name": params.name || '',
                "description": params.description,
                "image": params.image || '',
                "prefill": {
                    "email": params.email || '',
                    "contact": params.phonenumber || ''
                },
                "modal": {
                    "ondismiss": (/**
                     * @return {?}
                     */
                    function () {
                        reject('Razorpay modal closed');
                    })
                },
                handler: (/**
                 * @param {?} token
                 * @return {?}
                 */
                function (token) {
                    resolve({
                        "total_amount": params.amount,
                        "fee_amount": 0,
                        "net_amount": params.amount,
                        "currency": params.currency,
                        "trxn_id": token.razorpay_payment_id,
                        "raw_response": token
                    });
                    return false;
                })
            }
            // optional
            ;
            // optional
            if (params.order_id) {
                options.order_id = params.order_id;
            }
            self.handler = new window.Razorpay(options);
            self.handler.open();
        }));
    };
    /**
     * @return {?}
     */
    RazorpayService.prototype.onPopstate = /**
     * @return {?}
     */
    function () {
        this.handler.close();
    };
    RazorpayService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    RazorpayService.ctorParameters = function () { return []; };
    RazorpayService.propDecorators = {
        onPopstate: [{ type: HostListener, args: ['window:popstate',] }]
    };
    return RazorpayService;
}());
export { RazorpayService };
if (false) {
    /** @type {?} */
    RazorpayService.prototype.isAllowed;
    /** @type {?} */
    RazorpayService.prototype.addScript;
    /** @type {?} */
    RazorpayService.prototype.scriptReady;
    /** @type {?} */
    RazorpayService.prototype.handler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmF6b3JwYXkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYXltZW50anMvIiwic291cmNlcyI6WyJ2ZW5kb3IvcmF6b3JwYXkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxFQUFFLElBQUksSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2xDLE9BQU8sNEJBQTRCLENBQUE7QUFHbkM7SUFPRTtRQUxBLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsY0FBUyxHQUFZLEtBQUssQ0FBQztRQUMzQixnQkFBVyxHQUFZLElBQUksQ0FBQztRQUkxQixrQkFBa0I7SUFDakIsQ0FBQzs7OztJQUVKLGdDQUFNOzs7SUFBTjtRQUFBLGlCQU9DO1FBTkMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSTs7O1lBQUM7Z0JBQ3ZCLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixDQUFDLENBQUM7WUFDMUMsQ0FBQyxFQUFDLENBQUE7U0FDSDtJQUNILENBQUM7Ozs7SUFFRCxzQ0FBWTs7O0lBQVo7UUFDRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFDLE9BQU8sRUFBRSxNQUFNOztnQkFDN0IsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7WUFDdkQsZ0JBQWdCLENBQUMsR0FBRyxHQUFHLDhDQUE4QyxDQUFDO1lBQ3RFLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUM7WUFDbEMsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztZQUNsQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzlDLENBQUMsRUFBQyxDQUFBO0lBQ0osQ0FBQzs7Ozs7SUFFRCxvQ0FBVTs7OztJQUFWLFVBQVcsTUFBVztRQUNwQixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFDLE9BQU8sRUFBRSxNQUFNOztnQkFDN0IsWUFBWSxHQUFPLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7O2dCQUM3QyxVQUFVLEdBQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7O2dCQUNqRSxVQUFVLEdBQU8sQ0FBQyxZQUFZLEdBQUcsVUFBVSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUUzRCxPQUFPLENBQUM7Z0JBQ04sU0FBUyxFQUFFLEtBQUssR0FBRyxJQUFJLEVBQUU7Z0JBQ3pCLGNBQWMsRUFBRSxZQUFZO2dCQUM1QixZQUFZLEVBQUUsVUFBVTtnQkFDeEIsWUFBWSxFQUFFLFVBQVU7Z0JBQ3hCLFVBQVUsRUFBRSxLQUFLO2FBQ2xCLENBQUMsQ0FBQztRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxnQ0FBTTs7OztJQUFOLFVBQU8sTUFBVzs7WUFDWixJQUFJLEdBQVEsSUFBSTs7WUFDZCxNQUFNLEdBQUcsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxHQUFHLEdBQUc7UUFFekMsT0FBTyxJQUFJLE9BQU87Ozs7O1FBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTs7Z0JBQzdCLE9BQU8sR0FBUTtnQkFDakIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxRQUFRO2dCQUN0QixRQUFRLEVBQUUsTUFBTTtnQkFDaEIsVUFBVSxFQUFFLE1BQU0sQ0FBQyxRQUFRLElBQUksS0FBSztnQkFDcEMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxJQUFJLElBQUksRUFBRTtnQkFDekIsYUFBYSxFQUFFLE1BQU0sQ0FBQyxXQUFXO2dCQUNqQyxPQUFPLEVBQUUsTUFBTSxDQUFDLEtBQUssSUFBSSxFQUFFO2dCQUMzQixTQUFTLEVBQUU7b0JBQ1QsT0FBTyxFQUFFLE1BQU0sQ0FBQyxLQUFLLElBQUksRUFBRTtvQkFDM0IsU0FBUyxFQUFFLE1BQU0sQ0FBQyxXQUFXLElBQUksRUFBRTtpQkFDcEM7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLFdBQVc7OztvQkFBRTt3QkFDWCxNQUFNLENBQUMsdUJBQXVCLENBQUMsQ0FBQTtvQkFDakMsQ0FBQyxDQUFBO2lCQUNGO2dCQUNELE9BQU87Ozs7Z0JBQUUsVUFBUyxLQUFLO29CQUNyQixPQUFPLENBQUM7d0JBQ04sY0FBYyxFQUFFLE1BQU0sQ0FBQyxNQUFNO3dCQUM3QixZQUFZLEVBQUUsQ0FBQzt3QkFDZixZQUFZLEVBQUUsTUFBTSxDQUFDLE1BQU07d0JBQzNCLFVBQVUsRUFBRSxNQUFNLENBQUMsUUFBUTt3QkFDM0IsU0FBUyxFQUFFLEtBQUssQ0FBQyxtQkFBbUI7d0JBQ3BDLGNBQWMsRUFBRSxLQUFLO3FCQUN0QixDQUFDLENBQUM7b0JBQ0gsT0FBTyxLQUFLLENBQUM7Z0JBQ2YsQ0FBQyxDQUFBO2FBQ0Y7WUFFRCxXQUFXOztZQUFYLFdBQVc7WUFDWCxJQUFHLE1BQU0sQ0FBQyxRQUFRLEVBQUM7Z0JBQ2pCLE9BQU8sQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQzthQUNwQztZQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEIsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBR0Qsb0NBQVU7OztJQURWO1FBRUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQTtJQUN0QixDQUFDOztnQkE5RkYsVUFBVTs7Ozs7NkJBMkZSLFlBQVksU0FBQyxpQkFBaUI7O0lBSWpDLHNCQUFDO0NBQUEsQUEvRkQsSUErRkM7U0E5RlksZUFBZTs7O0lBQzFCLG9DQUEyQjs7SUFDM0Isb0NBQTJCOztJQUMzQixzQ0FBNEI7O0lBQzVCLGtDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSG9zdExpc3RlbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyB2NCBhcyB1dWlkIH0gZnJvbSAndXVpZCc7XG5pbXBvcnQgJ3VybC1zZWFyY2gtcGFyYW1zLXBvbHlmaWxsJ1xuZGVjbGFyZSB2YXIgd2luZG93OiBhbnk7XG7igItcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBSYXpvcnBheVNlcnZpY2Uge1xuICBpc0FsbG93ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgYWRkU2NyaXB0OiBib29sZWFuID0gZmFsc2U7XG4gIHNjcmlwdFJlYWR5OiBib29sZWFuID0gdHJ1ZTtcbiAgaGFuZGxlcjogYW55O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIC8vICB0aGlzLl9faW5pdCgpO1xuICAgICB9XG7igItcbiAgX19pbml0KCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5hZGRTY3JpcHQgJiYgdGhpcy5pc0FsbG93ZWQpIHtcbiAgICAgIHRoaXMuaW5qZWN0U2NyaXB0KCkudGhlbigoKSA9PiB7XG4gICAgICAgIHRoaXMuc2NyaXB0UmVhZHkgPSBmYWxzZTtcbiAgICAgICAgY29uc29sZS5sb2coJ3Jhem9ycGF5LmpzIHNjcmlwdCByZWFkeScpO1xuICAgICAgfSlcbiAgICB9XG4gIH1cblxuICBpbmplY3RTY3JpcHQoKSB7XG4gICAgdGhpcy5hZGRTY3JpcHQgPSB0cnVlO1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgc2NyaXB0dGFnRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5zcmMgPSAnaHR0cHM6Ly9jaGVja291dC5yYXpvcnBheS5jb20vdjEvY2hlY2tvdXQuanMnO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5vbmxvYWQgPSByZXNvbHZlO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5vbmVycm9yID0gcmVqZWN0O1xuICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChzY3JpcHR0YWdFbGVtZW50KTtcbiAgICB9KVxuICB9XG5cbiAgZmFrZUNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBsZXQgdG90YWxfYW1vdW50OmFueSA9IHBhcnNlSW50KHBhcmFtc1tcImFtb3VudFwiXSk7XG4gICAgICBsZXQgZmVlX2Ftb3VudDphbnkgPSAoKCgxMDAgKiAyLjkpIC8gdG90YWxfYW1vdW50KSArIDAuMzApLnRvRml4ZWQoMik7XG4gICAgICBsZXQgbmV0X2Ftb3VudDphbnkgPSAodG90YWxfYW1vdW50IC0gZmVlX2Ftb3VudCkudG9GaXhlZCgyKTtcbuKAi1xuICAgICAgcmVzb2x2ZSh7XG4gICAgICAgIFwidHJ4bl9pZFwiOiBcImNoX1wiICsgdXVpZCgpLFxuICAgICAgICBcInRvdGFsX2Ftb3VudFwiOiB0b3RhbF9hbW91bnQsXG4gICAgICAgIFwiZmVlX2Ftb3VudFwiOiBmZWVfYW1vdW50LFxuICAgICAgICBcIm5ldF9hbW91bnRcIjogbmV0X2Ftb3VudCxcbiAgICAgICAgXCJjdXJyZW5jeVwiOiBcInVzZFwiXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxu4oCLXG4gIGNoYXJnZShwYXJhbXM6IGFueSkge1xuICAgIGxldCBzZWxmOiBhbnkgPSB0aGlzO1xuICAgIGNvbnN0IGFtb3VudCA9IChwYXJhbXMuYW1vdW50IHx8IDApICogMTAwXG5cbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgbGV0IG9wdGlvbnM6IGFueSA9IHtcbiAgICAgICAgXCJrZXlcIjogcGFyYW1zLnVzZXJuYW1lLFxuICAgICAgICBcImFtb3VudFwiOiBhbW91bnQsXG4gICAgICAgIFwiY3VycmVuY3lcIjogcGFyYW1zLmN1cnJlbmN5IHx8IFwiSU5SXCIsXG4gICAgICAgIFwibmFtZVwiOiBwYXJhbXMubmFtZSB8fCAnJyxcbiAgICAgICAgXCJkZXNjcmlwdGlvblwiOiBwYXJhbXMuZGVzY3JpcHRpb24sXG4gICAgICAgIFwiaW1hZ2VcIjogcGFyYW1zLmltYWdlIHx8ICcnLFxuICAgICAgICBcInByZWZpbGxcIjoge1xuICAgICAgICAgIFwiZW1haWxcIjogcGFyYW1zLmVtYWlsIHx8ICcnLFxuICAgICAgICAgIFwiY29udGFjdFwiOiBwYXJhbXMucGhvbmVudW1iZXIgfHwgJydcbiAgICAgICAgfSxcbiAgICAgICAgXCJtb2RhbFwiOiB7XG4gICAgICAgICAgXCJvbmRpc21pc3NcIjogZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIHJlamVjdCgnUmF6b3JwYXkgbW9kYWwgY2xvc2VkJylcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGhhbmRsZXI6IGZ1bmN0aW9uKHRva2VuKSB7XG4gICAgICAgICAgcmVzb2x2ZSh7XG4gICAgICAgICAgICBcInRvdGFsX2Ftb3VudFwiOiBwYXJhbXMuYW1vdW50LFxuICAgICAgICAgICAgXCJmZWVfYW1vdW50XCI6IDAsXG4gICAgICAgICAgICBcIm5ldF9hbW91bnRcIjogcGFyYW1zLmFtb3VudCxcbiAgICAgICAgICAgIFwiY3VycmVuY3lcIjogcGFyYW1zLmN1cnJlbmN5LFxuICAgICAgICAgICAgXCJ0cnhuX2lkXCI6IHRva2VuLnJhem9ycGF5X3BheW1lbnRfaWQsXG4gICAgICAgICAgICBcInJhd19yZXNwb25zZVwiOiB0b2tlbiAgICBcbiAgICAgICAgICB9KTtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7ICAgICAgICAgIFxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIG9wdGlvbmFsXG4gICAgICBpZihwYXJhbXMub3JkZXJfaWQpe1xuICAgICAgICBvcHRpb25zLm9yZGVyX2lkID0gcGFyYW1zLm9yZGVyX2lkO1xuICAgICAgfVxuXG4gICAgICBzZWxmLmhhbmRsZXIgPSBuZXcgd2luZG93LlJhem9ycGF5KG9wdGlvbnMpO1xuICAgICAgc2VsZi5oYW5kbGVyLm9wZW4oKTtcbiAgICB9KTtcbiAgfVxu4oCLXG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpwb3BzdGF0ZScpXG4gIG9uUG9wc3RhdGUoKSB7XG4gICAgdGhpcy5oYW5kbGVyLmNsb3NlKClcbiAgfVxufSJdfQ==