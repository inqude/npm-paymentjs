/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { v4 as uuid } from 'uuid';
import { PaypalModalComponent } from './paypal.component';
import { NgxSmartModalService } from 'ngx-smart-modal';
import 'url-search-params-polyfill';
var PayPalService = /** @class */ (function () {
    function PayPalService(modalService) {
        this.modalService = modalService;
        this.isAllowed = false;
        this.addScript = false;
        this.scriptReady = true;
        this.__init();
    }
    /**
     * @return {?}
     */
    PayPalService.prototype.__init = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.addScript && this.isAllowed) {
            this.injectScript().then((/**
             * @return {?}
             */
            function () {
                _this.scriptReady = false;
                console.log('PayPal checkout.js script ready');
            }));
        }
    };
    /**
     * @param {?} params
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    PayPalService.prototype.config = /**
     * @param {?} params
     * @param {?} resolve
     * @param {?} reject
     * @return {?}
     */
    function (params, resolve, reject) {
        var _this = this;
        return {
            "locale": 'en_US',
            "env": params.isTest ? 'sandbox' : 'production',
            "style": {
                "layout": 'vertical',
            },
            "client": {
                "sandbox": params.username,
                "production": params.username,
            },
            "commit": true,
            "payment": (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            function (data, actions) {
                return actions.payment.create({
                    "payment": {
                        "transactions": [
                            {
                                "amount": {
                                    "total": params.amount,
                                    "currency": params.currency
                                },
                                "description": params.description || ''
                            },
                        ],
                    },
                });
            }),
            onAuthorize: (/**
             * @param {?} data
             * @param {?} actions
             * @return {?}
             */
            function (data, actions) {
                return actions.payment
                    .execute()
                    .then((/**
                 * @param {?} payment
                 * @return {?}
                 */
                function (payment) {
                    /** @type {?} */
                    var total_amount = params.amount;
                    /** @type {?} */
                    var fee_amount = 0;
                    /** @type {?} */
                    var net_amount = params.amount;
                    // check if the PayPal transaction didn't went through
                    if (payment.intent != 'sale' && payment.state != 'approved') {
                        reject(payment);
                    }
                    else {
                        resolve({
                            total_amount: total_amount,
                            fee_amount: fee_amount,
                            net_amount: net_amount,
                            currency: params.currency,
                            trxn_id: payment.id,
                        });
                        if (_this.modalRef && _this.modalRef.close) {
                            _this.modalRef.close();
                        }
                    }
                }))
                    .catch((/**
                 * @param {?} e
                 * @return {?}
                 */
                function (e) {
                    console.log(e);
                    reject(null);
                }));
            }),
        };
    };
    /**
     * @return {?}
     */
    PayPalService.prototype.injectScript = /**
     * @return {?}
     */
    function () {
        this.addScript = true;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var scripttagElement = document.createElement('script');
            scripttagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
            scripttagElement.onload = resolve;
            scripttagElement.onerror = reject;
            document.body.appendChild(scripttagElement);
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    PayPalService.prototype.fakeCharge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var total_amount = parseInt(params['amount']);
            /** @type {?} */
            var fee_amount = ((100 * 2.9) / total_amount + 0.3).toFixed(2);
            /** @type {?} */
            var net_amount = (total_amount - fee_amount).toFixed(2);
            resolve({
                trxn_id: 'ch_' + uuid(),
                total_amount: total_amount,
                fee_amount: fee_amount,
                net_amount: net_amount,
                currency: 'usd',
            });
        }));
    };
    /**
     * @param {?} params
     * @return {?}
     */
    PayPalService.prototype.charge = /**
     * @param {?} params
     * @return {?}
     */
    function (params) {
        var _this = this;
        /** @type {?} */
        var self = this;
        params.amount = params.amount || 0;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            if (!_this.addScript) {
                console.error('PayPal checkout.js script is not ready');
                return false;
            }
            /** @type {?} */
            var modalOptions = {
                escapable: true,
                customClass: 'paypalModalClass'
            };
            _this.modalRef = _this.modalService.create('PaypalModal', PaypalModalComponent, modalOptions).open();
            _this.modalRef.onOpenFinished.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                _this.modalService.setModalData({ isTest: params.isTest }, 'PaypalModal');
            }));
            _this.modalRef.onClose.subscribe((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                console.log('closed', result);
                reject();
            }));
            window.setTimeout((/**
             * @return {?}
             */
            function () {
                window.paypal.Button.render(self.config(params, resolve, reject), '#paypal-buttons-container');
            }), 1000);
        }));
    };
    PayPalService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    PayPalService.ctorParameters = function () { return [
        { type: NgxSmartModalService }
    ]; };
    return PayPalService;
}());
export { PayPalService };
if (false) {
    /** @type {?} */
    PayPalService.prototype.isAllowed;
    /** @type {?} */
    PayPalService.prototype.addScript;
    /** @type {?} */
    PayPalService.prototype.scriptReady;
    /** @type {?} */
    PayPalService.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalRef;
    /**
     * @type {?}
     * @private
     */
    PayPalService.prototype.modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGF5cGFsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGF5bWVudGpzLyIsInNvdXJjZXMiOlsidmVuZG9yL3BheXBhbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFVBQVUsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsRUFBRSxJQUFJLElBQUksRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUNoQyxPQUFPLEVBQUMsb0JBQW9CLEVBQUMsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUV2RCxPQUFPLDRCQUE0QixDQUFDO0FBRXBDO0lBUUUsdUJBQW9CLFlBQWtDO1FBQWxDLGlCQUFZLEdBQVosWUFBWSxDQUFzQjtRQU50RCxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGNBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsZ0JBQVcsR0FBWSxJQUFJLENBQUM7UUFLMUIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2hCLENBQUM7Ozs7SUFDRCw4QkFBTTs7O0lBQU47UUFBQSxpQkFPQztRQU5DLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDckMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDLElBQUk7OztZQUFDO2dCQUN2QixLQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztnQkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1lBQ2pELENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7Ozs7O0lBRUQsOEJBQU07Ozs7OztJQUFOLFVBQU8sTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNO1FBQTlCLGlCQTBEQztRQXpEQyxPQUFPO1lBQ0wsUUFBUSxFQUFFLE9BQU87WUFDakIsS0FBSyxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsWUFBWTtZQUMvQyxPQUFPLEVBQUU7Z0JBQ1AsUUFBUSxFQUFFLFVBQVU7YUFDckI7WUFDRCxRQUFRLEVBQUU7Z0JBQ1IsU0FBUyxFQUFFLE1BQU0sQ0FBQyxRQUFRO2dCQUMxQixZQUFZLEVBQUUsTUFBTSxDQUFDLFFBQVE7YUFDOUI7WUFDRCxRQUFRLEVBQUUsSUFBSTtZQUNkLFNBQVM7Ozs7O1lBQUUsVUFBQyxJQUFJLEVBQUUsT0FBTztnQkFDdkIsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQztvQkFDNUIsU0FBUyxFQUFFO3dCQUNULGNBQWMsRUFBRTs0QkFDZDtnQ0FDRSxRQUFRLEVBQUU7b0NBQ1IsT0FBTyxFQUFFLE1BQU0sQ0FBQyxNQUFNO29DQUN0QixVQUFVLEVBQUUsTUFBTSxDQUFDLFFBQVE7aUNBQzVCO2dDQUNELGFBQWEsRUFBRSxNQUFNLENBQUMsV0FBVyxJQUFJLEVBQUU7NkJBQ3hDO3lCQUNGO3FCQUNGO2lCQUNGLENBQUMsQ0FBQztZQUNMLENBQUMsQ0FBQTtZQUNELFdBQVc7Ozs7O1lBQUUsVUFBQyxJQUFJLEVBQUUsT0FBTztnQkFDekIsT0FBTyxPQUFPLENBQUMsT0FBTztxQkFDbkIsT0FBTyxFQUFFO3FCQUNULElBQUk7Ozs7Z0JBQUMsVUFBQSxPQUFPOzt3QkFDUCxZQUFZLEdBQUcsTUFBTSxDQUFDLE1BQU07O3dCQUM1QixVQUFVLEdBQUcsQ0FBQzs7d0JBQ2QsVUFBVSxHQUFHLE1BQU0sQ0FBQyxNQUFNO29CQUU5QixzREFBc0Q7b0JBQ3RELElBQUksT0FBTyxDQUFDLE1BQU0sSUFBSSxNQUFNLElBQUksT0FBTyxDQUFDLEtBQUssSUFBSSxVQUFVLEVBQUU7d0JBQzNELE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDakI7eUJBQU07d0JBQ0wsT0FBTyxDQUFDOzRCQUNOLFlBQVksRUFBRSxZQUFZOzRCQUMxQixVQUFVLEVBQUUsVUFBVTs0QkFDdEIsVUFBVSxFQUFFLFVBQVU7NEJBQ3RCLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUTs0QkFDekIsT0FBTyxFQUFFLE9BQU8sQ0FBQyxFQUFFO3lCQUNwQixDQUFDLENBQUM7d0JBRUgsSUFBRyxLQUFJLENBQUMsUUFBUSxJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFDOzRCQUN0QyxLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO3lCQUN2QjtxQkFDRjtnQkFDSCxDQUFDLEVBQUM7cUJBQ0QsS0FBSzs7OztnQkFBQyxVQUFBLENBQUM7b0JBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDZixNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2YsQ0FBQyxFQUFDLENBQUM7WUFDUCxDQUFDLENBQUE7U0FDRixDQUFDO0lBQ0osQ0FBQzs7OztJQUVELG9DQUFZOzs7SUFBWjtRQUNFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07O2dCQUM3QixnQkFBZ0IsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQztZQUN2RCxnQkFBZ0IsQ0FBQyxHQUFHLEdBQUcsK0NBQStDLENBQUM7WUFDdkUsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztZQUNsQyxnQkFBZ0IsQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1lBQ2xDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDOUMsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVELGtDQUFVOzs7O0lBQVYsVUFBVyxNQUFXO1FBQ3BCLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07O2dCQUM3QixZQUFZLEdBQVEsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQzs7Z0JBQzlDLFVBQVUsR0FBUSxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLFlBQVksR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOztnQkFDL0QsVUFBVSxHQUFRLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDNUQsT0FBTyxDQUFDO2dCQUNOLE9BQU8sRUFBRSxLQUFLLEdBQUcsSUFBSSxFQUFFO2dCQUN2QixZQUFZLEVBQUUsWUFBWTtnQkFDMUIsVUFBVSxFQUFFLFVBQVU7Z0JBQ3RCLFVBQVUsRUFBRSxVQUFVO2dCQUN0QixRQUFRLEVBQUUsS0FBSzthQUNoQixDQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRUQsOEJBQU07Ozs7SUFBTixVQUFPLE1BQVc7UUFBbEIsaUJBK0JDOztZQTlCSyxJQUFJLEdBQVEsSUFBSTtRQUNwQixNQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDO1FBQ25DLE9BQU8sSUFBSSxPQUFPOzs7OztRQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDakMsSUFBSSxDQUFDLEtBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ25CLE9BQU8sQ0FBQyxLQUFLLENBQUMsd0NBQXdDLENBQUMsQ0FBQztnQkFDeEQsT0FBTyxLQUFLLENBQUM7YUFDZDs7Z0JBRUssWUFBWSxHQUFRO2dCQUN4QixTQUFTLEVBQUUsSUFBSTtnQkFDZixXQUFXLEVBQUUsa0JBQWtCO2FBQ2hDO1lBRUQsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsb0JBQW9CLEVBQUUsWUFBWSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDbkcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsU0FBUzs7OztZQUFDLFVBQUMsTUFBTTtnQkFDNUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLE1BQU0sRUFBRSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1lBQzNFLENBQUMsRUFBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsU0FBUzs7OztZQUFDLFVBQUMsTUFBTTtnQkFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUE7Z0JBQzdCLE1BQU0sRUFBRSxDQUFDO1lBQ1gsQ0FBQyxFQUFDLENBQUM7WUFFSCxNQUFNLENBQUMsVUFBVTs7O1lBQUM7Z0JBQ2hCLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FDekIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxFQUNwQywyQkFBMkIsQ0FDNUIsQ0FBQztZQUNKLENBQUMsR0FBRSxJQUFJLENBQUMsQ0FBQztRQUNYLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Z0JBeklGLFVBQVU7Ozs7Z0JBSkYsb0JBQW9COztJQThJN0Isb0JBQUM7Q0FBQSxBQTFJRCxJQTBJQztTQXpJWSxhQUFhOzs7SUFDeEIsa0NBQTJCOztJQUMzQixrQ0FBMkI7O0lBQzNCLG9DQUE0Qjs7SUFDNUIsZ0NBQWE7Ozs7O0lBQ2IsaUNBQXNCOzs7OztJQUVWLHFDQUEwQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge3Y0IGFzIHV1aWR9IGZyb20gJ3V1aWQnO1xuaW1wb3J0IHtQYXlwYWxNb2RhbENvbXBvbmVudH0gZnJvbSAnLi9wYXlwYWwuY29tcG9uZW50JztcbmltcG9ydCB7IE5neFNtYXJ0TW9kYWxTZXJ2aWNlIH0gZnJvbSAnbmd4LXNtYXJ0LW1vZGFsJztcblxuaW1wb3J0ICd1cmwtc2VhcmNoLXBhcmFtcy1wb2x5ZmlsbCc7XG5kZWNsYXJlIHZhciB3aW5kb3c6IGFueTtcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBQYXlQYWxTZXJ2aWNlIHtcbiAgaXNBbGxvd2VkOiBib29sZWFuID0gZmFsc2U7XG4gIGFkZFNjcmlwdDogYm9vbGVhbiA9IGZhbHNlO1xuICBzY3JpcHRSZWFkeTogYm9vbGVhbiA9IHRydWU7XG4gIGhhbmRsZXI6IGFueTtcbiAgcHJpdmF0ZSBtb2RhbFJlZjogYW55O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgbW9kYWxTZXJ2aWNlOiBOZ3hTbWFydE1vZGFsU2VydmljZSkge1xuICAgIHRoaXMuX19pbml0KCk7XG4gIH1cbiAgX19pbml0KCk6IHZvaWQge1xuICAgIGlmICghdGhpcy5hZGRTY3JpcHQgJiYgdGhpcy5pc0FsbG93ZWQpIHtcbiAgICAgIHRoaXMuaW5qZWN0U2NyaXB0KCkudGhlbigoKSA9PiB7XG4gICAgICAgIHRoaXMuc2NyaXB0UmVhZHkgPSBmYWxzZTtcbiAgICAgICAgY29uc29sZS5sb2coJ1BheVBhbCBjaGVja291dC5qcyBzY3JpcHQgcmVhZHknKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIGNvbmZpZyhwYXJhbXMsIHJlc29sdmUsIHJlamVjdCkge1xuICAgIHJldHVybiB7XG4gICAgICBcImxvY2FsZVwiOiAnZW5fVVMnLFxuICAgICAgXCJlbnZcIjogcGFyYW1zLmlzVGVzdCA/ICdzYW5kYm94JyA6ICdwcm9kdWN0aW9uJyxcbiAgICAgIFwic3R5bGVcIjoge1xuICAgICAgICBcImxheW91dFwiOiAndmVydGljYWwnLFxuICAgICAgfSxcbiAgICAgIFwiY2xpZW50XCI6IHtcbiAgICAgICAgXCJzYW5kYm94XCI6IHBhcmFtcy51c2VybmFtZSxcbiAgICAgICAgXCJwcm9kdWN0aW9uXCI6IHBhcmFtcy51c2VybmFtZSxcbiAgICAgIH0sXG4gICAgICBcImNvbW1pdFwiOiB0cnVlLFxuICAgICAgXCJwYXltZW50XCI6IChkYXRhLCBhY3Rpb25zKSA9PiB7XG4gICAgICAgIHJldHVybiBhY3Rpb25zLnBheW1lbnQuY3JlYXRlKHtcbiAgICAgICAgICBcInBheW1lbnRcIjoge1xuICAgICAgICAgICAgXCJ0cmFuc2FjdGlvbnNcIjogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgXCJhbW91bnRcIjoge1xuICAgICAgICAgICAgICAgICAgXCJ0b3RhbFwiOiBwYXJhbXMuYW1vdW50LCBcbiAgICAgICAgICAgICAgICAgIFwiY3VycmVuY3lcIjogcGFyYW1zLmN1cnJlbmN5XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBcImRlc2NyaXB0aW9uXCI6IHBhcmFtcy5kZXNjcmlwdGlvbiB8fCAnJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgXSxcbiAgICAgICAgICB9LFxuICAgICAgICB9KTtcbiAgICAgIH0sXG4gICAgICBvbkF1dGhvcml6ZTogKGRhdGEsIGFjdGlvbnMpID0+IHtcbiAgICAgICAgcmV0dXJuIGFjdGlvbnMucGF5bWVudFxuICAgICAgICAgIC5leGVjdXRlKClcbiAgICAgICAgICAudGhlbihwYXltZW50ID0+IHtcbiAgICAgICAgICAgIGxldCB0b3RhbF9hbW91bnQgPSBwYXJhbXMuYW1vdW50O1xuICAgICAgICAgICAgbGV0IGZlZV9hbW91bnQgPSAwO1xuICAgICAgICAgICAgbGV0IG5ldF9hbW91bnQgPSBwYXJhbXMuYW1vdW50O1xuICAgICAgICAgICAgXG4gICAgICAgICAgICAvLyBjaGVjayBpZiB0aGUgUGF5UGFsIHRyYW5zYWN0aW9uIGRpZG4ndCB3ZW50IHRocm91Z2hcbiAgICAgICAgICAgIGlmIChwYXltZW50LmludGVudCAhPSAnc2FsZScgJiYgcGF5bWVudC5zdGF0ZSAhPSAnYXBwcm92ZWQnKSB7XG4gICAgICAgICAgICAgIHJlamVjdChwYXltZW50KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIHJlc29sdmUoe1xuICAgICAgICAgICAgICAgIHRvdGFsX2Ftb3VudDogdG90YWxfYW1vdW50LFxuICAgICAgICAgICAgICAgIGZlZV9hbW91bnQ6IGZlZV9hbW91bnQsXG4gICAgICAgICAgICAgICAgbmV0X2Ftb3VudDogbmV0X2Ftb3VudCxcbiAgICAgICAgICAgICAgICBjdXJyZW5jeTogcGFyYW1zLmN1cnJlbmN5LFxuICAgICAgICAgICAgICAgIHRyeG5faWQ6IHBheW1lbnQuaWQsXG4gICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgIGlmKHRoaXMubW9kYWxSZWYgJiYgdGhpcy5tb2RhbFJlZi5jbG9zZSl7XG4gICAgICAgICAgICAgICAgdGhpcy5tb2RhbFJlZi5jbG9zZSgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSlcbiAgICAgICAgICAuY2F0Y2goZSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhlKTtcbiAgICAgICAgICAgIHJlamVjdChudWxsKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH0sXG4gICAgfTtcbiAgfVxuXG4gIGluamVjdFNjcmlwdCgpIHtcbiAgICB0aGlzLmFkZFNjcmlwdCA9IHRydWU7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGxldCBzY3JpcHR0YWdFbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICBzY3JpcHR0YWdFbGVtZW50LnNyYyA9ICdodHRwczovL3d3dy5wYXlwYWxvYmplY3RzLmNvbS9hcGkvY2hlY2tvdXQuanMnO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5vbmxvYWQgPSByZXNvbHZlO1xuICAgICAgc2NyaXB0dGFnRWxlbWVudC5vbmVycm9yID0gcmVqZWN0O1xuICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChzY3JpcHR0YWdFbGVtZW50KTtcbiAgICB9KTtcbiAgfVxuXG4gIGZha2VDaGFyZ2UocGFyYW1zOiBhbnkpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgbGV0IHRvdGFsX2Ftb3VudDogYW55ID0gcGFyc2VJbnQocGFyYW1zWydhbW91bnQnXSk7XG4gICAgICBsZXQgZmVlX2Ftb3VudDogYW55ID0gKCgxMDAgKiAyLjkpIC8gdG90YWxfYW1vdW50ICsgMC4zKS50b0ZpeGVkKDIpO1xuICAgICAgbGV0IG5ldF9hbW91bnQ6IGFueSA9ICh0b3RhbF9hbW91bnQgLSBmZWVfYW1vdW50KS50b0ZpeGVkKDIpO1xuICAgICAgcmVzb2x2ZSh7XG4gICAgICAgIHRyeG5faWQ6ICdjaF8nICsgdXVpZCgpLFxuICAgICAgICB0b3RhbF9hbW91bnQ6IHRvdGFsX2Ftb3VudCxcbiAgICAgICAgZmVlX2Ftb3VudDogZmVlX2Ftb3VudCxcbiAgICAgICAgbmV0X2Ftb3VudDogbmV0X2Ftb3VudCxcbiAgICAgICAgY3VycmVuY3k6ICd1c2QnLFxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICBjaGFyZ2UocGFyYW1zOiBhbnkpIHtcbiAgICBsZXQgc2VsZjogYW55ID0gdGhpcztcbiAgICBwYXJhbXMuYW1vdW50ID0gcGFyYW1zLmFtb3VudCB8fCAwO1xuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBpZiAoIXRoaXMuYWRkU2NyaXB0KSB7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoJ1BheVBhbCBjaGVja291dC5qcyBzY3JpcHQgaXMgbm90IHJlYWR5Jyk7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cblxuICAgICAgY29uc3QgbW9kYWxPcHRpb25zOiBhbnkgPSB7XG4gICAgICAgIGVzY2FwYWJsZTogdHJ1ZSxcbiAgICAgICAgY3VzdG9tQ2xhc3M6ICdwYXlwYWxNb2RhbENsYXNzJ1xuICAgICAgfTtcblxuICAgICAgdGhpcy5tb2RhbFJlZiA9IHRoaXMubW9kYWxTZXJ2aWNlLmNyZWF0ZSgnUGF5cGFsTW9kYWwnLCBQYXlwYWxNb2RhbENvbXBvbmVudCwgbW9kYWxPcHRpb25zKS5vcGVuKCk7XG4gICAgICB0aGlzLm1vZGFsUmVmLm9uT3BlbkZpbmlzaGVkLnN1YnNjcmliZSgocmVzdWx0KSA9PiB7XG4gICAgICAgIHRoaXMubW9kYWxTZXJ2aWNlLnNldE1vZGFsRGF0YSh7IGlzVGVzdDogcGFyYW1zLmlzVGVzdCB9LCAnUGF5cGFsTW9kYWwnKTtcbiAgICAgIH0pO1xuICAgICAgXG4gICAgICB0aGlzLm1vZGFsUmVmLm9uQ2xvc2Uuc3Vic2NyaWJlKChyZXN1bHQpID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coJ2Nsb3NlZCcsIHJlc3VsdClcbiAgICAgICAgcmVqZWN0KCk7XG4gICAgICB9KTtcbiAgICAgIFxuICAgICAgd2luZG93LnNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB3aW5kb3cucGF5cGFsLkJ1dHRvbi5yZW5kZXIoXG4gICAgICAgICAgc2VsZi5jb25maWcocGFyYW1zLCByZXNvbHZlLCByZWplY3QpLFxuICAgICAgICAgICcjcGF5cGFsLWJ1dHRvbnMtY29udGFpbmVyJyxcbiAgICAgICAgKTtcbiAgICAgIH0sIDEwMDApO1xuICAgIH0pO1xuICB9XG59Il19